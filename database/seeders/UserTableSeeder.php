<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Core\Number;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User([
            'name' => 'Md Zaman Sarker',
            'name_bn' => 'মোঃ জামান সরকার',
            'email' => 'sarkerzaman@gmail.com',
            'mobile' => '01799838041',
            'email_verified_at' => now(),
            'password' => Hash::make('12345678'),
            'remember_token' => Str::random(10),
            'status' => 'active'
        ]);
        $admin->save();

        $admin = new User([
            'name' => 'Md Monir Hossain',
            'name_bn' => 'মোঃ মনির হোসেন',
            'email' => 'munir@orangebd.com',
            'mobile' => rand(00000000001, 99999999999),
            'email_verified_at' => now(),
            'password' => Hash::make('12345678'),
            'remember_token' => Str::random(10),
            'status' => 'active'
        ]);
        $admin->save();
    }
}
