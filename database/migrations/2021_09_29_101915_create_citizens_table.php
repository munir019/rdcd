<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitizensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizens', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->unique();
            $table->string('name');
            $table->string('name_en');
            $table->string('email')->unique();
            $table->string('nid')->unique();
            $table->string('nationality')->nullable();
            $table->string('mobile')->unique();
            $table->string('mother_name_en')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('father_name_en')->nullable();
            $table->string('spouse_name')->nullable();
            $table->string('spouse_name_en')->nullable();
            $table->string('gender')->nullable();
            $table->string('occupation')->nullable();
            $table->string('educational_qualification')->nullable();
            $table->string('religion')->nullable();
            $table->text('pre_address')->nullable();
            $table->text('per_address')->nullable();
            $table->text('photo')->nullable();
            $table->boolean('nid_verify')->default(0);
            $table->string('brn')->nullable();
            $table->boolean('brn_verify')->default(0);
            $table->string('passport')->nullable();
            $table->boolean('passport_verify')->default(0);
            $table->string('tin')->nullable();
            $table->boolean('tin_verify')->default(0);
            $table->date('date_of_birth');
            $table->string('token')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citizens');
    }
}
