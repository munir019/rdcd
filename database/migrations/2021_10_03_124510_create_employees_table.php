<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('username')->unique();
            $table->string('user_alias')->unique();
            $table->tinyInteger('user_role_id');
            $table->tinyInteger('is_admin')->default(0);
            $table->boolean('status');
            // $table->boolean('active')->default(0);
            $table->boolean('is_email_verified')->default(0);
            $table->string('gender')->nullable();
            $table->string('photo')->nullable();
            /**Employee Info*/
            $table->unsignedBigInteger('employee_id')->unique();
            $table->integer('is_cadre');
            $table->string('name_en')->nullable();
            $table->string('name_bn')->nullable();
            $table->string('father_name_en')->nullable();
            $table->string('father_name_bn')->nullable();
            $table->string('mother_name_en')->nullable();
            $table->string('mother_name_bn')->nullable();
            $table->string('spouse_name_en')->nullable();
            $table->string('spouse_name_bn')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('nid')->unique();
            $table->string('brn')->nullable();
            $table->string('passport')->nullable();
            $table->string('personal_email')->nullable();
            $table->string('personal_mobile')->unique();
            $table->integer('employee_grade')->nullable();
            $table->string('default_sign')->nullable();
            /**Office Info*/
            $table->unsignedBigInteger('office_info_id');
            $table->unsignedBigInteger('office_id');
            $table->unsignedBigInteger('office_unit_id');
            $table->unsignedBigInteger('office_unit_organogram_id');
            $table->string('designation_bn')->nullable();
            $table->string('designation_en')->nullable();
            $table->integer('designation_level');
            $table->integer('designation_sequence');
            $table->tinyInteger('office_head')->default(0);
            $table->string('incharge_label')->nullable();
            $table->date('joining_date')->nullable();
            $table->date('last_office_date')->nullable();
            //$table->boolean('show_unit');
            $table->string('unit_name_bn')->nullable();
            $table->string('unit_name_en')->nullable();
            $table->string('office_name_bn')->nullable();
            $table->string('office_name_en')->nullable();
            $table->tinyInteger('protikolpo_status')->default(0);
            /**Organogram Info*/
            $table->unsignedBigInteger('organogram_info_id');
            $table->unsignedBigInteger('superior_unit_id');
            $table->unsignedBigInteger('superior_designation_id');
            $table->unsignedBigInteger('ref_origin_unit_org_id');
            $table->unsignedBigInteger('ref_sup_origin_unit_desig_id');
            $table->unsignedBigInteger('ref_sup_origin_unit_id');
            $table->string('short_name_en')->nullable();
            $table->string('short_name_bn')->nullable();
            $table->string('designation_description')->nullable();
            $table->string('nationality')->nullable();
            $table->string('educational_qualification')->nullable();
            $table->string('pre_address')->nullable();
            $table->string('per_address')->nullable();
            $table->tinyInteger('is_unit_admin')->nullable();
            $table->tinyInteger('is_unit_head')->nullable();
            $table->tinyInteger('is_office_head')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
