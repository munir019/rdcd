<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminDashboardController;
use App\Http\Controllers\Admin\ComponentController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\CitizenController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RedirectToApp;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Auto Routes*/
$appUrl = rtrim(config('app.url'),'/');
$url = Url();
$parseUrl = parse_url($appUrl);
$tmpAppUrl = explode('/',$appUrl);
$tmpCurUrl = explode('/',$url->current());
$urlParts = array_slice($tmpCurUrl,count($tmpAppUrl));

$projectUrl = '';
$projectUrl = trim($appUrl, '/');
$projectUrl = preg_replace('#^https?://#', '', rtrim($projectUrl,'/'));
$projectUrl = ltrim(str_replace($parseUrl['host'],'',$projectUrl),'/');

$controller = 'HomeController';
$method = 'index';

if(isset($urlParts[0]) && !empty($urlParts[0]))
    $controller = ucfirst($urlParts[0]).'Controller';
if(isset($urlParts[1]))
    $method = $urlParts[1];
$controller = $controller.'@'.$method;
$namespace = 'App\\Http\\Controllers\\';
$admin_namespace = 'App\\Http\\Controllers\\Admin\\';
/*Auto Routes*/

Route::get('/', function () { return view('login'); });
Route::post('/', function () { return view('login'); });

Route::get($projectUrl.'/idplogin', $namespace.'AuthController@idplogin');
Route::get($projectUrl.'/citizen/login', $namespace.'AuthController@citizenLogin')->name('citizenlogin');
Route::get($projectUrl.'/employee/login', $namespace.'AuthController@employeeLogin')->name('employeelogin');
Route::post($projectUrl.'/rdcdauth', $namespace.'AuthController@rdcdAuth')->name('rdcdauth');
Route::post($projectUrl.'/idpauth', $namespace.'AuthController@idpauth');
Route::get($projectUrl.'/idplogout', $namespace.'AuthController@idplogout');

Route::get($projectUrl.'/doptorlogin', $namespace.'AuthController@doptorlogin');
Route::get($projectUrl.'/doptorauth', $namespace.'AuthController@doptorauth');
Route::get($projectUrl.'/doptorlogout', $namespace.'AuthController@doptorlogout');

Route::post($projectUrl.'/ndpotor', $namespace.'NdoptorController@index');

Route::get($projectUrl.'/changeLanguage/{lang}', $namespace.'HomeController@changeLanguage')->middleware('session.user');

Route::get('/home', [$namespace.HomeController::class, 'index'])->name('home');
// Route::get('/beneficiary/profile', [$namespace.BeneficiaryController::class, 'editProfile']);
// Route::resource('/beneficiary', $namespace.BeneficiaryController::class)->only(['index', 'edit', 'update', 'store']);
// Route::get('/employee/edit', [$namespace.EmployeeController::class, 'edit']);
// Route::resource('/employee', $namespace.EmployeeController::class)->only(['index', 'edit', 'update', 'store']);

Route::get('/admin/login', [AuthController::class, 'create'])->name('login');
Route::post('/admin/login', [AuthController::class, 'authenticate']);

Route::prefix('citizen')->name('citizen.')->group(function(){
    Route::middleware(['guest:citizen'])->group(function(){

    });

    Route::middleware(['session.user:citizen'])->group(function(){
        Route::get('/profile', [CitizenController::class, 'editProfile'])->name('edit');
        Route::put('/{id}', [CitizenController::class, 'update'])->name('update');
        Route::resource('/', CitizenController::class)->only(['index', 'store']);
        Route::get('/verify/{component}', [RedirectToApp::class, 'verifyUser'])->name('verify');
    });
});

Route::prefix('employee')->name('employee.')->group(function(){
    Route::middleware(['guest:employee'])->group(function(){

    });

    Route::middleware(['session.user:employee'])->group(function(){
        Route::get('/edit', [EmployeeController::class, 'edit'])->name('edit');
        Route::put('/{id}', [EmployeeController::class, 'update'])->name('update');
        Route::resource('/', EmployeeController::class)->only(['index', 'store']);
        Route::get('/verify/{component}', [RedirectToApp::class, 'verifyUser'])->name('verify');
    });
});


Route::group(['middleware' => 'admin:admin'], function () {

    //Route For Admin User
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/dashboard', [AdminDashboardController::class, 'index']);
        Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');

        Route::resource('/component', ComponentController::class);
        Route::resource('/user', UserController::class);
        //Route::post('/user/export', UserController::class);
        Route::get('/permission', [PermissionController::class, 'index'])->name('permission');
        Route::get('/permission/getPermissionList/{userType}/{userId}', [PermissionController::class, 'getPermissionByUserID']);
        Route::get('/permission/getUserList/{userType}', [PermissionController::class, 'getUserListByType']);
        Route::post('/permission/update-permission', [PermissionController::class, 'updatePermission']);

        // Route::get('/permission', [PermissionController::class, 'index']);
        // Route::get('/permission/add-permission', [PermissionController::class, 'addPermission']);
        // Route::post('/permission/store-permission', [PermissionController::class, 'savePermission']);


    });
});


//Route::any('{all}', $namespace.$controller)->where('all', '.*')->middleware('session.user');

//require __DIR__.'/auth.php';


/*use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BeneficiaryController;

Route::get('/', function () { return view('login'); });
Route::post('/', function () { return view('login'); });

Route::get('/idplogin',[AuthController::class, 'idplogin']);
Route::post('/idpauth',[AuthController::class, 'idpauth']);
Route::get('/idplogout',[AuthController::class, 'idplogout']);

Route::get('/changeLanguage/{lang}', [HomeController::class, 'changeLanguage']);


Route::get('/beneficiary/profile', [BeneficiaryController::class, 'editProfile']);
Route::resource('/beneficiary', BeneficiaryController::class)->only(['index', 'edit', 'update', 'store']);

Route::get('/users/profile', [UserController::class, 'editProfile']);
Route::resource('users', UserController::class)->only(['index', 'edit', 'update']);

Route::get('/dashboard', [HomeController::class, 'index']);


require __DIR__.'/auth.php';
*/
