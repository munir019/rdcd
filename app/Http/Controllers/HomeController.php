<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth']);
    }

    public function changeLanguage($lang){

        if (! in_array($lang, ['en', 'bn'])) {
            abort(400);
        }
        session(['locale' => $lang]);

        return back();
    }

    public function index(Request $request) {
        $user = [];
        if(session()->has('citizen')){
            $user = session()->get('citizen');
        }

        if(session()->has('employee')){
            $user = session()->get('employee');
        }

        /**For registered users*/
        // if(session()->has('user')){
        //     $user = session()->get('user');
        // }

        return view('home', compact('user'));
    }


}
