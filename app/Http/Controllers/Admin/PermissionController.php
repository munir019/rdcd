<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Employee;
use App\Models\Citizen;
use App\Models\Component;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userList = User::pluck('name', 'id')->toArray();
        $componentList = Component::pluck('title_en', 'id')->toArray();
        $users = User::with(['components'])->get();
        $employees = Employee::select(['id', 'employee_id', 'name_en as name'])->with(['components'])->get();
        $citizens = Citizen::select(['id', 'user_id', 'name_en as name'])->with(['components'])->get();

        return view('admin.permission.index', compact('userList', 'componentList', 'users', 'employees', 'citizens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $userList = User::pluck('name', 'id')->toArray();
        $componentList = Component::pluck('title_en', 'id')->toArray();

        return view('admin.permission.add-permission', compact('userList', 'componentList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->permissions);
        // $user = $request->user;
        // $permissions = $request->permissions;

        $rules = [
            'user' => 'required|numeric',
            'permissions.*' => 'required|numeric',
        ];

        $messages = [
            'user.required' => 'User is required',
            'user.numeric' => 'Please select user',
            'permissions.*.required' => 'Please select at least one component'
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }


        $numOfComponent = count($request->permissions);

        if($numOfComponent > 0 && ($request->permissions[0] != NULL)){
            for($i = 0; $i < $numOfComponent; $i++){
                Permission::create([
                    'user_id' => $request->user,
                    'component_id' => $request->permissions[$i]
                ]);
            }
        }

        return redirect('admin/permission')->with('success', 'Permission has been assigned to user');
    }

    public function getUserListByType($userType)
    {
        if($userType == 'employee'){
            $userList = Employee::pluck('name_en', 'id')->toArray();
        }else if($userType == 'citizen'){
            $userList = Citizen::pluck('name', 'id')->toArray();
        }else{
            $userList = User::pluck('name', 'id')->toArray();
        }

        $userListHTML = view('admin.permission.user-list', compact('userList'))->render();

        return response()->json(compact('userListHTML'));
    }

    public function getPermissionByUserID($permissionType, $permissionId)
    {
        $componentList = Component::pluck('title_en', 'id')->toArray();

        if($permissionType == 'admin'){
            $user = User::find($permissionId);
            $permissions = $user->components()->pluck('component_id')->toArray();
        }
        if($permissionType == 'employee'){
            $employee = Employee::find($permissionId);
            $permissions = $employee->components()->pluck('component_id')->toArray();
        }
        if($permissionType == 'citizen'){
            $citizen = Citizen::find($permissionId);
            $permissions = $citizen->components()->pluck('component_id')->toArray();
        }
        $userAccessHTML = view('admin.permission.user-access-list', compact('permissions', 'componentList'))->render();

        return response()->json(compact('userAccessHTML'));
    }

    public function updatePermission()
    {
        $rules = [
            'user_type' => 'required',
            'user' => 'required|numeric',
            'permissions.*' => 'required|numeric',
        ];

        $messages = [
            'user_type.required' => 'User type is required',
            'user.required' => 'User is required',
            'user.numeric' => 'Please select user',
            'permissions.*.required' => 'Please select at least one component'
        ];

        $validatedData = Validator::make(request()->all(), $rules, $messages);

        if($validatedData->passes()) {

            if(request()->user_type == 'admin'){
                $user = User::find(request()->user);
                $user->components()->detach();
                $user->components()->attach(request()->permissions);
            }

            if(request()->user_type == 'employee'){
                $employee = Employee::find(request()->user);
                $employee->components()->detach();
                $employee->components()->attach(request()->permissions);
            }

            if(request()->user_type == 'citizen'){
                $citizen = Citizen::find(request()->user);
                $citizen->components()->detach();
                $citizen->components()->attach(request()->permissions);
            }

            //Render updated users access information
            $componentList = Component::pluck('title_en', 'id')->toArray();
            if(request()->user_type == 'admin'){
                $users = User::with(['components'])->get();
            }
            if(request()->user_type == 'employee'){
                $users = Employee::with(['components'])->get();
            }
            if(request()->user_type == 'citizen'){
                $users = Citizen::with(['components'])->get();
            }
            $allUserAccessHTML = view('admin.permission.all-users-access-list', compact('users', 'componentList'))->render();

            return response()->json(compact('allUserAccessHTML'));
        }

        return response()->json(['error' => $validatedData->error()->all()]);
    }


}
