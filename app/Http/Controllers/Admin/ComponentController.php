<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Component;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
//use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ComponentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth('admin')->check()){
            $components = Component::select('*')->orderBy('id')->get();
        }
        return view('admin.component.index', compact('components'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.component.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:3|unique:components,name',
            'title_en' => 'required|string|min:3',
            'title_bn' => 'required|string|min:2',
            'icon' => 'mimes:jpeg,jpg,bmp,png,gif'
        ];

        $messages = [
            'name.required' => 'Component name is required',
            'name.unique' => 'Component name already exist',
            'title_en.required' => 'Component title is required',
            'title_bn.required' => 'Component title is required',
            'icon.mimes' => 'Icon should be in jpeg/png/gif format'
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        $data = $request->all();
        $data['icon'] = $request->icon ? $request->file('icon')->getClientOriginalName():'';

        $filename = null;
        if($request->hasFile('icon')){
            $path = public_path().'/images/components';
            if (!is_dir($path)) {
                Storage::makeDirectory($path);
            }

            $filename = $request->file('icon')->getClientOriginalName();
            $request->file('icon')->move($path, $filename);
        }

        if(Component::create($data)){
            return redirect('admin/component')->with('success', 'Item details has been added');
        }
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function show(Component $component)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function edit(Component $component)
    {
        return view('admin.component.edit', compact('component'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Component $component)
    {
        $rules = [
            'name' => ['required','string','min:3', Rule::unique('components')->ignore($component->id)],
            'title_en' => 'required|string|min:3',
            'title_bn' => 'required|string|min:2',
            'icon' => 'mimes:jpeg,jpg,bmp,png,gif'
        ];

        $messages = [
            'name.required' => 'Component name is required',
            'name.unique' => 'Component name already exist',
            'title_en.required' => 'Component title is required',
            'title_bn.required' => 'Component title is required',
            'icon.mimes' => 'Icon should be in jpeg/png/gif format'
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        $data = [
            'title_bn' => $request['title_bn'],
            'title_en' => $request['title_en'],
            'name' => $request['name'],
            'url' => $request['url'],
            'status' => $request['status']
        ];

        $data['icon'] = $request->icon ? $request->file('icon')->getClientOriginalName(): $component->icon;

        $filename = null;
        if($request->hasFile('icon')){
            $path = public_path().'/images/components';
            if (!is_dir($path)) {
                Storage::makeDirectory($path);
            }

            $filename = $request->file('icon')->getClientOriginalName();
            $request->file('icon')->move($path, $filename);
        }

        if(Component::where('id', $component->id)->update($data)){
            return redirect('admin/component')->with('success', 'Item details has been updated');
        }
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function destroy(Component $component)
    {
        if($component->delete()){
            if($component->icon){
                if(file_exists(public_path().'/images/components/' .$component->icon))
                    unlink(public_path().'/images/components/'.$component->icon);
            }
        }
        return redirect('admin/component')->with('success', 'component '.$component->name.' has been deleted');
    }
}
