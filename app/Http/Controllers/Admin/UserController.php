<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="user/'.$row->id.'/edit" class="btn-edit"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i></a>
                                  <form style="display:inline" id="deleteForm-'.$row->id.'" action="admin/user/'.$row->id.'" method="POST"><a href="javascript:void(0)" data-id="'.$row->id.'"><i class="fa fa-times fa-1x" aria-hidden="true"></i></a></form>';
                                //<a href="javascript:void(0)" data-id="'.$row->id.'" class="btn-delete"><i class="fa fa-times fa-1x" aria-hidden="true"></i></a>';
                                //@method("delete") @csrf
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'name_bn' => 'required|string',
            'email' => 'required|email|unique:users',
            'mobile' => 'numeric',
            'status' => 'required|string',
            'password' => 'required|min:6,max:15',
        ];

        $messages = [
            'name.required' => 'User is required',
            'name.string' => 'User name must be a string',
            'name_bn.required' => 'User is required',
            'name_bn.string' => 'User name must be a string',
            'email.required' => 'Email is required',
            'email.email' => 'Email must be a valid email',
            'email.unique' => 'Email must be a unique',
            'mobile.numeric' => 'Mobile should be numeric',
            'status.required' => 'User status is required',
            'password.required' => 'Password is required',
            'password.min' => 'Password length should be between 6 to 15 characters',
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        $data = $request->all();
        $data['photo'] = $request->photo ? $request->file('photo')->getClientOriginalName() : '';
        $data['password'] = Hash::make($request->password);

        if(User::Create($data)){
            if($request->hasFile('photo')){
                $path = public_path().'/images/users';
                if(!is_dir($path)){
                    Storage::makeDirectory($path);
                }
                $filename = $request->file('photo')->getClientOriginalName();
                $request->file('photo')->move($path, $filename);
            }
            return redirect('admin/user')->with('success', 'New user has been created');
        }
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin/user/edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|string',
            'name_bn' => 'required|string',
            'email' => 'required|email',
            'mobile' => 'numeric',
            'status' => 'required|string',
        ];

        $messages = [
            'name.required' => 'User is required',
            'name.string' => 'User name must be a string',
            'name_bn.required' => 'User is required',
            'name_bn.string' => 'User name must be a string',
            'email.required' => 'Email is required',
            'email.email' => 'Email must be a valid email',
            //'email.unique' => 'Email must be a unique',
            'mobile.numeric' => 'Mobile should be numeric',
            'status.required' => 'User status is required',
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        $data['name'] = $request->name;
        $data['name_bn'] = $request->name_bn;
        $data['email'] = $request->email;
        $data['mobile'] = $request->mobile;
        $data['status'] = $request->status;
        $data['photo'] = $request->photo ? $request->file('photo')->getClientOriginalName() : $request->photo;
        $data['password'] = Hash::make($request->password);

        if(User::where('id', $id)->update($data)){

            if($request->hasFile('photo')){
                $path = public_path().'/images/users';
                if(!is_dir($path)){
                    Storage::makeDirectory($path);
                }
                $filename = $request->file('photo')->getClientOriginalName();
                $request->file('photo')->move($path, $filename);
            }
            return redirect('admin/user')->with('success', 'User information has been updated');
        }
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if($user->delete()){
            if($user->photo){
                if(file_exists(public_path().'/images/users/' .$user->photo))
                    unlink(public_path().'/images/users/'.$user->photo);
            }
        }
        return redirect('admin/user')->with('success', 'User '.$user->name.' has been deleted');
    }
}
