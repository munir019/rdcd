<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['admin']);
    }

    public function changeLanguage($lang){

        if (! in_array($lang, ['en', 'bn'])) {
            abort(400);
        }
        session(['locale' => $lang]);

        return back();
    }

    public function index(Request $request) {
        //dd(auth('admin')->user());
        //$user =  $request->session()->get('user');

        return view('dashboard');
    }


}
