<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public function create()
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::guard('admin')->attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('admin/dashboard');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function citizenLogin(){
        return view('auth.citizenlogin');
    }

    public function employeeLogin(){
        return view('auth.employeelogin');
    }

    public function rdcdAuth(Request $request)
    {
        $rules = [
            'username' => 'required|min:11',
            'password' => 'required',
        ];

        $messages = [
            'username.required' => 'Username is required',
            'username.min' => 'Mobile number should be exactly 11 digits',
            'password.required' => 'Password is required',
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        if(request()->get('usertype') == 'citizen')
        {
            $credentials = ['mobile' => $request->username, 'password' => $request->password];

            if (Auth::guard('citizen')->attempt($credentials)) {
                $request->session()->regenerate();
                session()->put('citizen', (object) auth('citizen')->user()->toArray());
                return redirect()->intended('home');
            }
        }

        if(request()->get('usertype') == 'employee')
        {
            $credentials = ['username' => $request->username, 'password' => $request->password];

            if (Auth::guard('employee')->attempt($credentials)) {
                $request->session()->regenerate();
                session()->put('employee', (object) auth('employee')->user()->toArray());
                return redirect()->intended('home');
            }
        }

        return back()->withErrors([
            'error' => 'The provided credentials do not match our records.',
        ]);
    }

    public function idplogin()
    {
        $client = new \IDP_Client();
        $client->setClientId('cnwsJowjT0mUgDNq6rKl');
        $client->setClientSecret('3Lj1bo7vxVtfGhhxtZcO.MnhFeLPNqV');
        $client->setRedirectUri('http://rdcd.gov.bd/idpauth');

        $authUrl = $client->loginRequest();
        header("Location: ".$authUrl);
        die();
    }

    public function idpauth(Request $request){
        $client = new \IDP_Client();
        $client->setClientId('cnwsJowjT0mUgDNq6rKl');
        $client->setClientSecret('3Lj1bo7vxVtfGhhxtZcO.MnhFeLPNqV');
        $data = $client->responseRequest($_POST);

        if(isset($data->id)){
            $data->type = 'citizen';
            $request->session()->put('citizen', $data);
            return redirect(config('app.url').'citizen/profile');
        }
        return redirect(config('app.url').'home');
    }

    public function idplogout(){
        $client = new \IDP_Client();
        $client->setClientId('cnwsJowjT0mUgDNq6rKl');
        $client->setClientSecret('3Lj1bo7vxVtfGhhxtZcO.MnhFeLPNqV');
        $client->setRedirectUri('http://rdcd.gov.bd');
        $authUrl = $client->logoutRequest();

        session()->forget('citizen');
        header("Location: ".$authUrl);
        //Auth::logout();
        die();
    }

    public function doptorLogin(){
        return redirect()->to(env('DOPTOR_SSO_LOGIN_URL').'?referer='.base64_encode(env('APP_LOGIN_URL')));
    }

    public function doptorAuth(Request $request)
    {
        $user_doptor_info=null;
        try{
            if(!isset($request->data)){
                return redirect(config('app.url'));
            }
            $user_doptor_info=json_decode(gzuncompress(base64_decode($request->data)));
            if(!isset($user_doptor_info->status) || $user_doptor_info->status!="success"){
                return redirect(config('app.url'));
            }
            $user = $user_doptor_info=$user_doptor_info->user_info;
        }catch(Exception $ex){
            return redirect(config('app.url'));
        }

        /*$user=$user_doptor_info->user;
        $employee_info=$user_doptor_info->employee_info;

        $office_info=$user_doptor_info->office_info;
        $organogram_info=$user_doptor_info->organogram_info;

        if(empty($office_info) || empty( $organogram_info)){
            return $this->appLogout();
        }

        foreach ($office_info as $office_info_key => $office_info_value) {
            if($office_info_value->status){
                $office_info=$office_info_value;
                break;
            }
        }

        if(isset($office_info->office_id)){
            $oisf = new Oisf;
            $ministry_id = $oisf->data('office?id='. $office_info->office_id.'&xgselect=ministry');
            $ministry_id = json_decode($ministry_id, true);

            foreach ($ministry_id as $ministry_key => $ministry_value) {
                $ministry_id = $ministry_value;
                break;
            }
        }

        $ministry_id = isset($ministry_id['ministry'])?$ministry_id =$ministry_id['ministry']:null;
        $OISF_V2 = new OISF_V_2;
        $n_doptor_token=$OISF_V2->getToken($user->username);


        $user = array(
            "userName" => $employee_info->name_bng,//'আবুল কাসেম',
            "employeeRecordId" => $employee_info->id,//17378
            "id" => $user->id,
            "officeId" =>  $office_info->office_id,//2213,
            "designation" => $office_info->designation,//'প্রশাসনিক কর্মকর্তা',
            "officeUnitId" => $office_info->office_unit_id,//19372,
            "InchargeLabel" => $office_info->incharge_label,//0,
            "officeUnitOrganogramId" => $office_info->office_unit_organogram_id,//60666,
            "officeNameEng" => $office_info->office_name_en,//'Ministry of liberation War Affairs',
            "officeNameBng" => $office_info->office_name_bn,//'মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়',
            "officeMinistryId" => $ministry_id,//$office_info->officeMinistryId,//48, //TODO
            "officeMinistryNameEng" => $office_info->office_name_en,//'Ministry of Liberation War Affairs',
            "officeMinistryNameBng" => $office_info->office_name_bn,//'মুক্তিযুদ্ধ বিষয়ক মন্ত্রণালয়',
            "unitNamEng" => $office_info->unit_name_en,
            "unitNameBng" => $office_info->unit_name_bn,//'আইসিটি শাখা',
            "office_ministry" => $ministry_id,//$office_info->office_ministry,//48, //TODO
            "office_officeoriginunitid" => 7399,//$office_info->office_officeoriginunitid,//7399, //TODO
            "office_office" => $office_info->office_id,//2213,
            "office_parentunit" => $office_info->office_unit_id,//19371,
            "is_admin" =>  $user->is_admin,
            'doptor_user_name'=>$user->username,
            'n_doptor_token'=>$n_doptor_token,
        );
        */

        $user = $this->getFormatedDoptorUser($user);
        session()->put('employee', $user);
        return redirect(config('app.url').'employee/edit');
    }

    public function doptorLogout()
    {
        session()->forget('employee');
        //Auth::logout();
        return redirect()->away('https://n-doptor-accounts-stage.nothi.gov.bd/logout');
    }

    /**Format  Doptor Employee Data Before Set in Session*/
    protected function getFormatedDoptorUser($employee){

        $employee = collect($employee);
        $userinfo = array($employee['user']);
        $employee_info = $employee['employee_info'];
        $office_info = $employee['office_info'];
        $organogram_info = collect($employee['organogram_info'])[$office_info[0]->office_unit_organogram_id];

        $user = collect($userinfo)->map(function ($user) use($employee_info, $office_info, $organogram_info){
            return [
                'user_id' => $user->id,
                'username' => $user->username,
                'user_alias' => $user->user_alias,
                'user_role_id' => $user->user_role_id,
                'is_admin' => $user->is_admin,
                'status' => $user->user_status,
                'is_email_verified' => $user->is_email_verified,
                'photo' => $user->photo,
                'employee_id' => $employee_info->id,
                'name_en' => $employee_info->name_eng,
                'name_bn' => $employee_info->name_bng,
                'father_name_en' => $employee_info->father_name_eng,
                'father_name_bn' => $employee_info->father_name_bng,
                'mother_name_en' => $employee_info->mother_name_eng,
                'mother_name_bn' => $employee_info->mother_name_bng,
                'date_of_birth' => $employee_info->date_of_birth,
                'nid' => $employee_info->nid,
                'brn' => $employee_info->bcn,
                'passport' => $employee_info->ppn,
                'personal_email' => $employee_info->personal_email,
                'personal_mobile' => $employee_info->personal_mobile,
                'is_cadre' => $employee_info->is_cadre,
                'employee_grade' => $employee_info->employee_grade,
                'joining_date' => $employee_info->joining_date,
                'default_sign' => $employee_info->default_sign,
                'office_info_id' => $office_info[0]->id,
                'office_id' => $office_info[0]->office_id,
                'office_unit_id' => $office_info[0]->office_unit_id,
                'office_unit_organogram_id' => $office_info[0]->office_unit_organogram_id,
                'designation_bn' => $office_info[0]->designation,
                'designation_en' => $office_info[0]->designation_en,
                'designation_level' => $office_info[0]->designation_level,
                'designation_sequence' => $office_info[0]->designation_sequence,
                'office_head' => $office_info[0]->office_head,
                'incharge_label' => $office_info[0]->incharge_label,
                'joining_date' => $office_info[0]->joining_date? date( 'Y-m-d', strtotime($office_info[0]->joining_date)):'',
                'last_office_date' => $office_info[0]->last_office_date? date( 'Y-m-d', strtotime($office_info[0]->last_office_date)) : '',
                'unit_name_bn' => $office_info[0]->unit_name_bn,
                'unit_name_en' => $office_info[0]->unit_name_en,
                'office_name_bn' => $office_info[0]->office_name_bn,
                'office_name_en' => $office_info[0]->office_name_en,
                'protikolpo_status' => $office_info[0]->protikolpo_status,
                'organogram_info_id' => $organogram_info->id,
                'superior_unit_id' => $organogram_info->superior_unit_id,
                'superior_designation_id' => $organogram_info->superior_designation_id,
                'ref_origin_unit_org_id' => $organogram_info->ref_origin_unit_org_id,
                'ref_sup_origin_unit_desig_id' => $organogram_info->ref_sup_origin_unit_desig_id,
                'ref_sup_origin_unit_id' => $organogram_info->ref_sup_origin_unit_id,
                'short_name_en' => $organogram_info->short_name_eng,
                'short_name_bn' => $organogram_info->short_name_bng,
                'designation_description' => $organogram_info->designation_description,
                'is_unit_admin' => $organogram_info->is_unit_admin,
                'is_unit_head' => $organogram_info->is_unit_head,
                'is_office_head' => $organogram_info->is_office_head,
                'created_by' => $organogram_info->created_by,
                'modified_by' => $organogram_info->modified_by,
                'type' => 'employee'
            ];
        })->first();

        return $user = (object) $user;
    }
}
