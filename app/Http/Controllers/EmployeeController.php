<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Validator;

class EmployeeController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'name_en' => 'required|string|min:3',
            'name_bn' => 'required|string|min:3',
            //'personal_email' => 'email',
            'personal_mobile' => 'required|string|min:11',
            'nid' => 'required|digits_between:10, 17',
            //'brn' => 'string|digits_between:17, 21',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            // 'photo' => 'mimes:jpeg,jpg,bmp,png,gif',
            // 'pre_address' => 'string',
            // 'per_address' => 'string'
        ];

        $messages = [
            'name_bn.required' => 'Beneficiary name is required',
            'name_en.required' => 'Beneficiary name is required',
            'personal_mobile.required' => 'Mobile number is required',
            // 'mother_name_bn.min' => 'Mother name should be minimum 3 characters',
            // 'mother_name_en.min' => 'Mother name should be minimum 3 characters',
            // 'father_name_bn.min' => 'Father name should be minimum 3 characters',
            // 'father_name_en.min' => 'Father name should be minimum 3 characters',
            'nid.required' => 'NID is required',
            'nid.digits_between' => 'NID must be between 10 to 17 digits',
            //'brn.digits' => 'BRN must be at least 17 digits',
            'date_of_birth.required' => 'Birth date is required'
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }
        $employee = Employee::where('employee_id', $request->get('employee_id'))->firstOrFail();

        if($employee->update($request->all())){
            return redirect('home')->with('success', 'Employee details has been added');
        }
        return redirect()->back()->withInput();
    }

    public function edit(Employee $employee)
    {
        $user = session()->get('employee');

        if(!isset($user->employee_id)){
            return redirect(config('app.url').'home');
        }

        if (Employee::where('employee_id', $user->employee_id)->exists()) {
            $rdcd_user = Employee::where('employee_id', $user->employee_id)->first()->toArray();

            $filtered_user = array_filter((array) $user, function($val){
                return $val == true || $val === 0;
            });

            $user = (object) array_merge($filtered_user, $rdcd_user);
        }else{
            $this->saveEmployeeInfo($user);
            $user->gender = $user->nationality = $user->pre_address = $user->per_address = '';
        }

        return view('employee.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name_en' => 'required|string|min:3',
            'name_bn' => 'required|string|min:3',
            //'personal_email' => 'email',
            'personal_mobile' => 'required|string|min:11',
            'nid' => 'required|digits_between:10, 17',
            //'brn' => 'string|digits_between:17, 21',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            // 'photo' => 'mimes:jpeg,jpg,bmp,png,gif',
            // 'pre_address' => 'string',
            // 'per_address' => 'string'
        ];

        $messages = [
            'name_bn.required' => 'Beneficiary name is required',
            'name_en.required' => 'Beneficiary name is required',
            'personal_mobile.required' => 'Mobile number is required',
            // 'mother_name_bn.min' => 'Mother name should be minimum 3 characters',
            // 'mother_name_en.min' => 'Mother name should be minimum 3 characters',
            // 'father_name_bn.min' => 'Father name should be minimum 3 characters',
            // 'father_name_en.min' => 'Father name should be minimum 3 characters',
            'nid.required' => 'NID is required',
            'nid.digits_between' => 'NID must be between 10 to 17 digits',
            //'brn.digits' => 'BRN must be at least 17 digits',
            'date_of_birth.required' => 'Birth date is required'
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        $employee = Employee::where('id', $id)->first();
        $updateStatus = $employee->update($request->all());

        if($updateStatus == true){
            return redirect('home')->with('success', 'Employee details has been updated');
        }
        return redirect()->back()->withInput();
    }

    public function saveEmployeeInfo($data){
        $employee = new Employee;
        $employee->user_id = $data->user_id;
        $employee->username = $data->username;
        $employee->user_alias = $data->user_alias;
        $employee->user_role_id = $data->user_role_id;
        $employee->is_admin = $data->is_admin;
        $employee->status = $data->status;
        $employee->is_email_verified = $data->is_email_verified;
        $employee->employee_id = $data->employee_id;
        $employee->name_bn = $data->name_bn;
        $employee->name_en = $data->name_en;
        $employee->personal_email = $data->personal_email;
        $employee->personal_mobile = $data->personal_mobile;
        $employee->father_name_bn = $data->father_name_bn;
        $employee->father_name_en = $data->father_name_en;
        $employee->mother_name_bn = $data->mother_name_bn;
        $employee->mother_name_en = $data->mother_name_en;
        $employee->short_name_bn = $data->short_name_bn;
        $employee->short_name_en = $data->short_name_en;
        $employee->designation_description = $data->designation_description;

        $employee->photo = $data->photo;
        $employee->brn = $data->brn;
        $employee->passport = $data->passport;
        $employee->nid = $data->nid;
        $employee->date_of_birth = $data->date_of_birth;

        $employee->is_cadre = $data->is_cadre;
        $employee->employee_grade = $data->employee_grade;
        $employee->joining_date = $data->joining_date? date( 'Y-m-d', strtotime($data->joining_date)) : null;
        $employee->office_info_id = $data->office_info_id;
        $employee->office_id = $data->office_id;
        $employee->office_unit_id = $data->office_unit_id;
        $employee->office_unit_organogram_id = $data->office_unit_organogram_id;
        $employee->designation_bn = $data->designation_bn;
        $employee->designation_en = $data->designation_en;
        $employee->designation_level = $data->designation_level;
        $employee->designation_sequence = $data->designation_sequence;
        $employee->office_head = $data->office_head;
        $employee->incharge_label = $data->incharge_label;
        $employee->last_office_date = $data->last_office_date? date( 'Y-m-d', strtotime($data->last_office_date)) : null;
        $employee->unit_name_bn = $data->unit_name_bn;
        $employee->unit_name_en = $data->unit_name_en;
        $employee->office_name_bn = $data->office_name_bn;
        $employee->office_name_en = $data->office_name_en;

        $employee->organogram_info_id = $data->organogram_info_id;
        $employee->superior_unit_id = $data->superior_unit_id;
        $employee->superior_designation_id = $data->superior_designation_id;
        $employee->ref_origin_unit_org_id = $data->ref_origin_unit_org_id;
        $employee->ref_sup_origin_unit_desig_id = $data->ref_sup_origin_unit_desig_id;
        $employee->ref_sup_origin_unit_id = $data->ref_sup_origin_unit_id;
        $employee->is_unit_admin = $data->is_unit_admin;
        $employee->is_unit_head = $data->is_unit_head;
        $employee->is_office_head = $data->is_office_head;
        $employee->protikolpo_status = $data->protikolpo_status;
        $employee->default_sign = $data->default_sign;

        $employee->created_by = $data->created_by;
        $employee->modified_by = $data->modified_by;
        $employee->password = Hash::make('12345678');

        if($employee->save()) return true;
    }
}
