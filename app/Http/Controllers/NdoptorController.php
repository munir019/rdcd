<?php

namespace App\Http\Controllers;

use App\Orangebd;
use Illuminate\Http\Request;

class NdoptorController extends Controller
{
    public function index(Request $request){

        header('Content-Type: application/json');
        if($request->isMethod('post')) {
            $nDoptor = new Orangebd\nDoptor();

            $path = $request->api;
            $param = $request->all();
            unset($param['api']);
            $param = http_build_query($param);

            return $nDoptor->data($path . '?' . $param);
        }
        return '';
        die();
    }
}