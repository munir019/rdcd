<?php

namespace App\Http\Controllers;

use App\Models\Citizen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
//use App\Http\Requests\Citizen\Update;
use Validator;

class CitizenController extends Controller
{

    public function index()
    {
        //
    }

    public function editProfile(Request $request, Citizen $citizen)
    {
        $user = $request->session()->get('citizen');

        if(!isset($user->id)){
            return redirect(config('app.url').'home');
        }

        if (Citizen::where('user_id', $user->id)->exists()) {
            $rdcd_user = Citizen::where('user_id', $user->id)->first()->toArray();

            $filtered_user = array_filter((array) $user, function($val){
                return $val == true || $val === 0;
            });
            $user = (object) array_merge($filtered_user, $rdcd_user);
        }else{
            $this->saveCitizenInfo($user);
            $user->nationality = '';
            $user->user_id = $user->id;
            $user->id = '';
        }
        return view('citizen.edit', compact('user'));
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|min:3',
            'name_en' => 'required|string|min:3',
            'email' => 'email',
            'mobile' => 'required|string|min:11',
            'mother_name' => 'string|min:3',
            'mother_name_en' => 'string|min:3',
            'father_name' => 'string|min:3',
            'father_name_en' => 'string|min:3',
            'nid' => 'required|digits_between:10, 17',
            'nid_verify' => 'boolean',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            // 'photo' => 'mimes:jpeg,jpg,bmp,png,gif',
            // 'pre_address' => 'string',
            // 'per_address' => 'string'
        ];

        $messages = [
            'name.required' => 'Citizen name is required',
            'name_en.required' => 'Citizen name is required',
            'mobile.required' => 'Mobile number is required',
            'mother_name.required' => 'Mother name is required',
            'mother_name_en.required' => 'Mother name is required',
            'father_name.required' => 'Father name is required',
            'father_name_en.required' => 'Father name is required',
            // 'gender.required' => 'Gender is required',
            // 'nationality.required' => 'Nationality is required',
            'nid.required' => 'NID is required',
            'nid.digits_between' => 'NID must be between 10 to 17 digits',
            // 'brn.digits' => 'BRN must be at least 17 digits',
            'date_of_birth.required' => 'Birth date is required',
            //'photo' => 'mimes:jpeg,jpg,bmp,png,gif'
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        $citizen = Citizen::where('user_id', $request->get('user_id'))->firstOrFail();

        if($citizen->update($request->all())){
            return redirect('home')->with('success', 'Citizen details has been updated');
        }
        return redirect()->back()->withInput();
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|string|min:3',
            'name_en' => 'required|string|min:3',
            'email' => 'email',
            'mobile' => 'required|string|min:11',
            'mother_name' => 'string|min:3',
            'mother_name_en' => 'string|min:3',
            'father_name' => 'string|min:3',
            'father_name_en' => 'string|min:3',
            'nid' => 'required|digits_between:10, 17',
            'nid_verify' => 'boolean',
            'date_of_birth' => 'required|date|date_format:Y-m-d',
            // 'photo' => 'mimes:jpeg,jpg,bmp,png,gif',
            // 'pre_address' => 'string',
            // 'per_address' => 'string'
        ];

        $messages = [
            'name.required' => 'Citizen name is required',
            'name_en.required' => 'Citizen name is required',
            'mobile.required' => 'Mobile number is required',
            'mother_name.required' => 'Mother name is required',
            'mother_name_en.required' => 'Mother name is required',
            'father_name.required' => 'Father name is required',
            'father_name_en.required' => 'Father name is required',
            // 'gender.required' => 'Gender is required',
            // 'nationality.required' => 'Nationality is required',
            'nid.required' => 'NID is required',
            'nid.digits_between' => 'NID must be between 10 to 17 digits',
            // 'brn.digits' => 'BRN must be at least 17 digits',
            'date_of_birth.required' => 'Birth date is required',
            //'photo' => 'mimes:jpeg,jpg,bmp,png,gif'
        ];

        $validatedData = Validator::make($request->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        $citizen = Citizen::findOrFail($id);
        $updateStatus = $citizen->update($request->all());

        if($updateStatus == true){
            return redirect('home')->with('success', 'Citizen details has been updated');
        }
        return redirect()->back()->withInput();
    }

    public function saveCitizenInfo($data){
        $citizen = new Citizen;
        $citizen->user_id = $data->id;
        $citizen->name = $data->name;
        $citizen->name_en = $data->name_en;
        $citizen->email = $data->email;
        $citizen->mobile = $data->mobile;
        $citizen->mother_name = $data->mother_name;
        $citizen->mother_name_en = $data->mother_name_en;
        $citizen->father_name = $data->father_name;
        $citizen->father_name_en = $data->father_name_en;
        $citizen->spouse_name = $data->spouse_name;
        $citizen->spouse_name_en = $data->spouse_name_en;
        $citizen->gender = $data->gender;
        $citizen->occupation = $data->occupation;
        $citizen->religion = $data->religion;
        $citizen->nationality = $data->nationality;
        $citizen->pre_address = $data->pre_address;
        $citizen->per_address = $data->per_address;
        $citizen->photo = $data->photo;
        $citizen->nid_verify = $data->nid_verify;
        $citizen->brn = $data->brn;
        $citizen->brn_verify = $data->brn_verify;
        $citizen->passport = $data->passport;
        $citizen->nid = $data->nid;
        $citizen->passport_verify = $data->passport_verify;
        $citizen->tin = $data->tin;
        $citizen->tin_verify = $data->tin_verify;
        $citizen->date_of_birth = $data->date_of_birth;
        $citizen->token = $data->token;
        $citizen->password = Hash::make('12345678');

        if($citizen->save()) return true;
    }
}
