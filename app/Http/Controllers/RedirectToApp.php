<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Component;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Http;
use Firebase\JWT\Key;

class RedirectToApp extends Controller
{
    public function verifyUser($id){

        if(session()->has('citizen')){
            $data = session()->get('citizen');

        }else if(session()->has('employee')){
            $data = session()->get('employee');
        }else{
            return redirect()->back();
        }


        $component = Component::where('id', $id)->first();
        $key = 'er@rdcd@t0ken';

        $data->photo = '';
        $token = JWT::encode($data, $key,'HS256');
        $url = $component->url.'?token='.$token;

        // return Http::post($component->url, [
        //     'token' => $token
        // ]);
        header("Location:".$url);
        exit;
    }
}
