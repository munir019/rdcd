<?php

namespace App\Http\Requests\Citizen;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Citizen;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Citizen $citizen)
    {
        $rules = [
            'name' => 'required|string|min:3',
            'name_en' => 'required|string|min:3',
            'email' => ['email', Rule::unique('beneficiaries')->ignore($citizen->id)],
            'mobile' => ['required|string|min:11,max:17', Rule::unique('beneficiaries')->ignore($citizen->id)],
            'mother_name' => 'string|min:3',
            'mother_name_en' => 'string|min:3',
            'father_name' => 'string|min:3',
            'father_name_en' => 'string|min:3',
            'spouse_name' => 'string|min:3',
            'spouse_name_en' => 'string|min:3',
            'gender' => 'string|min:4',
            'occupation' => 'string|min:3',
            'religion' => 'string|min:3',
            'nid' => 'required| |digits_between:10, 17',
            'nid_verify' => 'required|boolean',
            'brn' => 'string|digits_between:17, 21',
            'brn_verify' => 'boolean',
            'tin' => 'digits:12, 12',
            'tin_verify' => 'boolean',
            'passport' => 'string',
            'passport_verify' => 'boolean',
            'date_of_birth' => 'required|date',
            'photo' => 'mimes:jpeg,jpg,bmp,png,gif',
        ];

        return $rules;
    }
}
