<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class SessionUser
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
        if(session()->has("citizen") || session()->has("employee")) {
            return $next($request);
        }
        //return abort(404);
        return  redirect(config('app.url').'login');
    }
}
