<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth('admin')->user() && $request->path() == 'admin/dashboard'){
            $user = new User();
            $user->id = auth('admin')->user()->id;
            $user->name = auth('admin')->user()->name;
            $user->email = auth('admin')->user()->email;
            $user->type = 'admin';
            session()->put('user', $user->toArray());
        }

        if(!auth('admin')->check()){
            return redirect(config('app.url').'admin/login');
        }
        return $next($request);
    }
}
