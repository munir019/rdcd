<?php
use App\Models\Component;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Employee;
use App\Models\Citizen;

class Custom
{
    public static function getUserComponents(){
        // $lang = session()->get('locale') ?? 'bn';
        // App::setLocale($lang);

        $components = [];
        if(session()->has('citizen')){
            if (Citizen::where('user_id', session()->get('citizen')->id)->exists()) {
                $components = (Citizen::with(['components'])->where('user_id', session()->get('citizen')->id)->firstOrFail())->components;
            }
        }else if(session()->has('employee')){
            if (Employee::where('employee_id', session()->get('employee')->employee_id)->exists()) {
                $components = (Employee::with(['components'])->where('employee_id', session()->get('employee')->employee_id)->firstOrFail())->components;
            }
        }else{
            $components = (User::with(['components'])->where('id', auth('admin')->user()->id)->firstOrFail())->components;
        }
        return $components;
    }

}
