<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $lang = config('app.locale');
        session(['locale' => $lang]);
        $baseUrl = config('app.url');
        $userType = ['admin'=>'Admin', 'employee'=>'Govt Employee', 'citizen'=>'Citizen'];

        view()->share('baseUrl', $baseUrl);
        view()->share('userType', $userType);
    }
}
