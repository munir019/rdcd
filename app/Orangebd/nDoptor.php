<?php

namespace App\Orangebd;

class nDoptor
{
    private $token;
    // private $siteLink = 'https://n-doptor-api-stage.nothi.gov.bd/api';
    // private $client_id = 'mgeksheba';
    // private $password = '123456';
    private $siteLink = 'https://n-doptor-api.nothi.gov.bd/api';
    private $client_id = 'mygov';
    private $password = 'W62@/p53c7JKf';
    private $username = '1';
    private $param = array();

    public function data($param){

        $this->createToken();
        return $this->getData($this->siteLink.'/v1/'.$param);
    }

    private function getData($url){
        $result = array();
        if($this->token!='') {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 0);
            $headers = array(
                'Content-Type:application/json',
                'Authorization: Bearer ' . $this->token,
                'api-version:1'
            );

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch);
            curl_close($ch);
            //$result = json_decode($result, true);
            //$result = json_encode($result,JSON_UNESCAPED_UNICODE);
        }
        return $result;
    }

    private function createToken(){
        $url = $this->siteLink.'/client/login';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"client_id=".$this->client_id."&password=".$this->password."&username=".$this->username);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result=curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result,true);

        $this->token = '';
        if(isset($result) && $result['status']=="success"&& isset($result['data']))
            $this->token = $result['data']['token'];
    }
}
