<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;

class Citizen extends Model implements AuthenticatableContract
{
    use HasFactory, Authenticatable;

    public function components(){
        return $this->morphToMany(Component::class, 'permission')->withTimestamps();
    }

    protected $fillable = ['user_id', 'name', 'name_en', 'email', 'mobile', 'nid', 'mother_name_en', 'mother_name', 'father_name',
                            'father_name_en', 'spouse_name_en', 'spouse_name', 'gender', 'occupation', 'religion', 'nationality',
                            'pre_address', 'per_address', 'photo', 'nid_verify', 'brn', 'brn_verify', 'passport', 'educational_qualification',
                            'passport_verify', 'tin', 'tin_verify', 'date_of_birth'
                        ];

    // public function getIncrementing()
    // {
    //     return false;
    // }

    // public function getKeyType()
    // {
    //     return 'string';
    // }
}
