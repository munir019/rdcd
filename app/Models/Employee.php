<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;


class Employee extends Model implements AuthenticatableContract
{
    use HasFactory, Authenticatable;

    protected $fillable = ['user_id', 'username', 'user_alias', 'user_role_id', 'is_admin', 'status', 'is_email_verified',
                            'employee_id', 'name_bn', 'name_en', 'personal_email', 'personal_mobile', 'nid', 'brn', 'ppn',
                            'mother_name_en', 'mother_name_bn', 'father_name_bn', 'father_name_en', 'spouse_name_en', 'spouse_name',
                            'date_of_birth', 'gender', 'occupation', 'religion', 'is_cadre', 'employee_grade', 'joining_date',
                            'office_info_id', 'office_id', 'office_unit_id', 'office_unit_organogram_id', 'designation_bn', 'designation_en',
                            'designation_level', 'designation_sequence', 'office_head', 'incharge_label', 'last_office_date',
                            'unit_name_bn', 'unit_name_en', 'office_name_bn', 'office_name_en', 'protikolpo_status', 'organogram_info_id',
                            'superior_unit_id', 'superior_designation_id', 'ref_origin_unit_org_id', 'ref_sup_origin_unit_desig_id',
                            'ref_sup_origin_unit_id', 'short_name_en', 'short_name_bn', 'designation_description', 'is_unit_admin',
                            'is_unit_head', 'is_office_head', 'created_by', 'modified_by', 'default_sign', 'photo', 'pre_address', 'per_address',
                            'nationality', 'educational_qualification'
                        ];

    // public function components(){
    //     return $this->belongsToMany(Component::class, 'permissions')
    //                 ->withPivot('user_type')
    //                 ->withTimestamps();
    // }

    public function components(){
        return $this->morphToMany(Component::class, 'permission')
                    ->withTimestamps();
    }

}
