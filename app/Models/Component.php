<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Component extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'title_bn', 'title_en', 'icon', 'url', 'status'];

    // public function users(){
    //     return $this->belongsToMany(User::class, 'permissions')->withTimestamps();
    // }

    public function users(){
        return $this->morphedByMany(User::class, 'permission');
    }

    public function employees(){
        return $this->morphedByMany(Employee::class, 'permission');
    }

    public function citizens(){
        return $this->morphedByMany(Citizen::class, 'permission');
    }
}
