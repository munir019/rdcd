<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'language' => 'বাংলা',

    /**Component Name*/
    'loan_and_capital_management' => 'ঋণ এবং মূলধন ব্যবস্থাপনা',
    'cooperative_society_registration_and_monitoring_management' => 'সমবায় সমিতি নিবন্ধন ও পর্যবেক্ষণ ব্যবস্থাপনা',
    'online_milk_collection_and_distribution' => 'অনলাইন দুধ সংগ্রহ ও বিতরণ ব্যবস্থাপনা',
    'sales_ecommerce' => 'বিক্রয় ও ই-কমার্স সিস্টেম',
    'citizen_training_and_skill_development_management' => 'সুবিধাভোগী প্রশিক্ষণ ও দক্ষতা উন্নয়ন ব্যবস্থাপনা',
    'citizen_information_and_service_management' => 'বেনিফিশিয়ারি ইনফরমেশন অ্যান্ড সার্ভিস ম্যানেজমেন্ট সিস্টেম',

    /**Admin Menu*/
    'component' => 'কম্পোনেন্ট',
    'component_list' => 'কম্পোনেন্ট তালিকা',
    'assign_permission' => 'অনুমতি প্রদান',

    /**User Profile*/
    'user' => 'ব্যবহারকারী',
    'citizen_profile' => 'সেবাগ্রহীতার প্রোফাইল',
    'personal_information' => 'ব্যাক্তিগত তথ্য',
    'workplace_information' => 'কর্মস্থলের তথ্য',
    'communication_information' => 'যোগাযোগের তথ্য',
    'information' => 'তথ্য',
    'address' => 'ঠিকানা',
    'documents' => 'ডকুমেন্টস',
    'attachment' => 'সংযুক্তি',
    'name_bengali' => 'নাম (বাংলায়)',
    'name_english' => 'নাম (ইংরেজিতে)',
    'mobile' => 'মোবাইল',
    'email' => 'ইমেইল',
    'gender' => 'জেন্ডার',
    'mother_name' => 'মাতার নাম (বাংলায়)',
    'mother_name_en' => 'মাতার নাম (ইংরেজিতে)',
    'father_name' => 'পিতার নাম (বাংলায়)',
    'father_name_en' => 'পিতার নাম (ইংরেজিতে)',
    'spouse_name' => 'স্বামী/স্ত্রী নাম (বাংলায়)',
    'spouse_name_en' => 'স্বামী/স্ত্রী নাম (ইংরেজিতে)',
    'date_of_birth' => 'জন্ম তারিখ',
    'occupation' => 'পেশা/পদবি',
    'educational_qualification' => 'শিক্ষাগত যোগ্যতা',
    'nationality' => 'জাতীয়তা',
    'nid' => 'জাতীয় পরিচয়পত্র',
    'religion' => 'ধর্ম',
    'passport_number' => 'পাসপোর্ট নম্বর',
    'birth_certificate_no' => 'জন্মনিবন্ধন সনদ',
    'driving_licence_no' => 'ড্রাইভিং লাইসেন্স নম্বর',
    'tin_no' => 'টিন নম্বর',
    'permanent_address' => 'স্থায়ী ঠিকানা',
    'present_address' => 'বর্তমান ঠিকানা',
    'work_address' => 'কর্মস্থলের ঠিকানা',
    'photo' => 'ছবি',
    'nid_verify' => 'জাতীয় পরিচয়পত্র যাচাই করুন',

    /**Employee Profile*/
    'employee_information' => 'কর্মচারীর তথ্য',
    'office_information' => 'অফিসের তথ্য',
    'designation' => 'পদবী',
    'short_name' => 'সংক্ষিপ্ত নাম',
    'office_name' => 'অফিসের নাম',
    'unit_name' => 'ইউনিটের নাম',
    'joining_date' => 'যোগদানের তারিখ',
    'last_office_date' => 'অফিসের শেষ তারিখ',
    'incharge_label' => 'ভারপ্রাপ্ত',
    'designation_level' => 'পদবী স্তর',
    'designation_sequence' => 'পদবী ক্রম',


    /**Menu Item*/
    'profile' => 'প্রোফাইল',
    'myprofile' => 'আমার প্রোফাইল',
    'account_settings' => 'অ্যাকাউন্ট সেটিংস',


    'notification' => 'নোটিফিকেশন',
    'user_notification' => 'সেবাগ্রহীতার নোটিফিকেশন',
    'no_notification' => 'কোন নোটিফিকেশন নেই!',
    'services_widget' => 'অন্যান্য সেবাসমূহ',


    'about' => 'আমাদের কথা',
    'contact' => 'যোগাযোগ',
    'contact_us' => 'যোগাযোগ',
    'faq' => 'আপনার জিজ্ঞাসা',
    'home' => 'হোম',
    'about_us' => 'আমাদের পরিচিতি',
    'quick_links' => 'কুইক লিঙ্ক',
    'services' => 'সেবা',

    'learn_more' => 'আরও পড়ুন',
    'login' => 'লগইন',
    'citizen_login' => 'নাগরিক লগইন',
    'employee_login' => 'কর্মচারী লগইন',
    'USERNAME' => 'ব্যবহারকারীর মোবাইল',
    'EMAIL_MOBILE_NID' => 'মোবাইল/ইমেল/এনআইডি',
    'PASSWORD' => 'পাসওয়ার্ড',
    'CITIZEN_LOGIN' => 'নাগরিক লগইন',
    'EMPLOYEE_LOGIN' => 'কর্মচারী লগইন',
    'logout' => 'লগ আউট',
    'register_now' => 'নিবন্ধন করুন',
    'subscribe' => 'সাবস্ক্রাইব করুন',
    'no_account_yet' => 'আপনি এখনও অ্যাকাউন্ট করেন নি?',
    'have_an_account' => 'ইতিমধ্যে একাউন্ট আছে ?',
    'lost_your_password' => 'আপনার পাসওয়ার্ড হারিয়েছেন?',
    'submit' => 'Submit',

    /**Admin Dashboard */
    'dashboard' => 'ড্যাশবোর্ড',
    'component_dashboard' => 'কম্পোনেন্ট ড্যাশবোর্ড',
    'user_count' => 'ব্যবহারকারীর সংখ্যা: ',
];
