<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    /**Component Name*/
    'loan_and_capital_management' => 'Loan and Capital Management',
    'cooperative_society_registration_and_monitoring_management' => 'Cooperative Society Registration & Monitoring Management',
    'online_milk_collection_and_distribution' => 'Online Milk Collection & Distribution Management',
    'sales_ecommerce' => 'Sales & E-Commerce',
    'citizen_information_and_service_management' => 'Citizen Information & Service Management',
    'citizen_training_and_skill_development_management' => 'Citizen Training & Skill Development Management',

    /**Admin Menu*/
    'component' => 'Component',
    'component_list' => 'Component List',
    'assign_permission' => 'Assign Permission',

    /**User Profile*/
    'user' => 'User',
    'citizen_profile' => 'Citizen Profile',
    'personal_information' => 'Persoanl Information',
    'workplace_information' => 'Workplace Information',
    'communication_information' => 'Communication Information',
    'information' => 'Information',
    'address' => 'Address',
    'documents' => 'Documents',
    'attachment' => 'Attachments',
    'name_bengali' => 'নাম (বাংলায়)',
    'name_english' => 'নাম (ইংরেজিতে)',
    'mobile' => 'মোবাইল',
    'email' => 'ইমেইল',
    'gender' => 'জেন্ডার',
    'mother_name' => 'মাতার নাম (বাংলায়)',
    'mother_name_en' => 'মাতার নাম (ইংরেজিতে)',
    'father_name' => 'পিতার নাম (বাংলায়)',
    'father_name_en' => 'পিতার নাম (ইংরেজিতে)',
    'spouse_name' => 'স্বামী/স্ত্রী নাম (বাংলায়)',
    'spouse_name_en' => 'স্বামী/স্ত্রী নাম (ইংরেজিতে)',
    'date_of_birth' => 'জন্ম তারিখ',
    'occupation' => 'পেশা/পদবি',
    'educational_qualification' => 'Educational Qualification',
    'nationality' => 'জাতীয়তা',
    'nid' => 'জাতীয় পরিচয়পত্র',
    'religion' => 'ধর্ম',
    'passport_number' => 'পাসপোর্ট নম্বর',
    'birth_certificate_no' => 'জন্মনিবন্ধন সনদ',
    'driving_licence_no' => 'ড্রাইভিং লাইসেন্স নম্বর',
    'tin_no' => 'TIN Number',
    'permanent_address' => 'Permanent Address',
    'present_address' => 'Present Address',
    'work_address' => 'Work Place',
    'photo' => 'Profile Image',
    'nid_verify' => 'Verify NID',

    /**Employee Profile*/
    'employee_information' => 'Employee Information',
    'office_information' => 'Office Information',
    'designation' => 'Designation',
    'short_name' => 'Short Name',
    'office_name' => 'Office Name',
    'unit_name' => 'Unit Name',
    'joining_date' => 'Joining Date',
    'last_office_date' => 'Last Office Date',
    'incharge_label' => 'Incharge',
    'designation_level' => 'Designation Level',
    'designation_sequence' => 'Designation Sequence',

    /**Menu  Items*/
    'profile' => 'Profile',
    'myprofile' => 'My Profile',
    'account_settings' => 'Account Settings',


    'user_notification' => 'citizen Notification',
    'notification' => 'Notification',
    'no_notification' => 'There is no notification!',
    'services_widget' => 'Other Services',



    'language' => 'English',
    'about' => 'About',
    'contact' => 'Contact',
    'contact_us' => 'Contact Us',
    'faq' => 'FAQ',
    'home' => 'Home',
    'about_us' => 'About Us',
    'quick_links' => 'Quick Links',
    'services' => 'Services',

    'learn_more' => 'Learn More',
    'login' => 'Login',
    'citizen_login' => 'Citizen Login',
    'employee_login' => 'Employee Login',
    'USERNAME' => 'Username',
    'EMAIL_MOBILE_NID' => 'Mobile/Email/NID',
    'CITIZEN_LOGIN' => 'Citizen Login',
    'EMPLOYEE_LOGIN' => 'Employee Login',
    'PASSWORD' => 'Password',
    'logout' => 'Log Out',
    'register_now' => 'Register Now',
    'subscribe' => 'Subscribe',
    'no_account_yet' => 'Didn\'t you account yet ?',
    'have_an_account' => 'Already have an account ?',
    'lost_your_password' => 'Lost your password ?',
    'submit' => 'Submit',

    /**Admin Dashboard */
    'dashboard' => 'Dashboard',
    'component_dashboard' => 'Component dashboard',
    'user_count' => 'User Count: ',
];
