<div class="col-lg-12" id="permissionInfo">
    @foreach ($componentList as $key=>$value)
        {!! Form::checkbox('permissions[]', $key, in_array($key, $permissions)? true: false, ['class'=>'checkbox-item']) !!}
        <span class="checkbox-value">{{ $value }}</span>
    @endforeach
</div>
