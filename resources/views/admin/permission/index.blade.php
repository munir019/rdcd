@extends('layouts.backendapp')
@section('page_title', 'Assign User Permission to Component')

@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-md-11">
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Assign User Permission to Component
								</h3>
							</div>
						</div>
					</div>
                    <!--begin::Form-->
					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="assign_permission" action="#" enctype="multipart/form-data" method="POST">
						@csrf
						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    {{-- <label for="user">Select User <span class="text-danger">*</span></label> --}}
                                    {!! Form::select('user_type', $userType, null, ['class' => 'form-control', 'id' => 'user_type_id', 'placeholder' => '---Select User Type---']) !!}
                                    @error('user')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    {{-- <label for="user">Select User <span class="text-danger">*</span></label> --}}
                                    {!! Form::select('user', $userList, null, ['class' => 'form-control', 'id' => 'user_id', 'placeholder' => '---Select User---']) !!}
                                    @error('user')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
							</div>

							<div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                     <fieldset class="well component-list">
                                        <legend class="well-legend">Select Component</legend>
                                        <div class="col-lg-12" id="permissionInfo">
                                            {{-- <label for="icon">Select Components</label> --}}
                                            @foreach ($componentList as $key=>$value)
                                                {!! Form::checkbox('permissions[]', $key, null, ['class'=>'checkbox-item']) !!}
                                                <span class="checkbox-value">{{ $value }}</span>
                                            @endforeach
                                      </fieldset>
                                </div>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" class="btn btn-form-submit">
									Give Permission
								</button>
								<a href="{{ route('permission') }}" class="btn btn-secondary">Cancel</a>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
            <div class="col-md-11" id="user_components">
                @if ($users)
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped align-middle" id="update_user_data">
                            <thead>
                                <tr>
                                    <th class="text-center">User Name</th>
                                    @foreach ($componentList as $key=>$value)
                                        <th>{{ $value }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                            @if($users || $employees || $citizens)
                                    @if ($users)
                                        @foreach ($users as $user)
                                        <tr>
                                            <td class="text-center">{{ $user->name }} (admin)</td>
                                            @php
                                                $hasPermissions = $user->components->pluck('id')->toArray();
                                            @endphp
                                            @foreach ($componentList as $key=>$value)
                                                <td>
                                                    <input type="checkbox" class="checkbox-item" {{ in_array($key, $hasPermissions)?'checked':'' }} disabled />
                                                </td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    @endif

                                    @if($employees)
                                        @foreach ($employees as $employee)
                                        <tr>
                                            <td class="text-center">{{ $employee->name }} (employee)</td>
                                            @php
                                                $hasPermissions = $employee->components->pluck('id')->toArray();
                                            @endphp
                                            @foreach ($componentList as $key=>$value)
                                                <td>
                                                    <input type="checkbox" class="checkbox-item" {{ in_array($key, $hasPermissions)?'checked':'' }} disabled />
                                                </td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    @endif

                                    @if($citizens)
                                        @foreach ($citizens as $citizen)
                                        <tr>
                                            <td class="text-center">{{ $citizen->name }} (citizen)</td>
                                            @php
                                                $hasPermissions = $citizen->components->pluck('id')->toArray();
                                            @endphp
                                            @foreach ($componentList as $key=>$value)
                                                <td>
                                                    <input type="checkbox" class="checkbox-item" {{ in_array($key, $hasPermissions)?'checked':'' }} disabled />
                                                </td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    @endif

                                @else
                                    <tr>
                                        <td colspan="6">{{ __("There is no component available") }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                @endif
            </div>
		</div>
	</div>

    @section('scripts')
    <script>
    $(document).ready(function() {

        $('#user_type_id').change(function(){
            if($(this).val() != ''){
                $.ajax({
                    url: "{{url('admin/permission/getUserList')}}/"+$(this).val(),
                    dataType: 'JSON',
                    success: function(data) {
                        $('#user_id').html(data.userListHTML);
                    },
                    error: function (xhr, status, error){
                        if(status == 'error')
                            console.log(error);
                    }
                });
            }
	    });

        $('#user_id').change(function(){
            if($(this).val() != '' && $('#user_type_id').val() != ''){
                $.ajax({
                    url: "{{url('admin/permission/getPermissionList')}}/"+$('#user_type_id').val()+"/"+$(this).val(),
                    dataType: 'JSON',
                    success: function(data) {
                        $('#permissionInfo').html(data.userAccessHTML);
                    },
                    error: function (xhr, status, error){
                        if(status == 'error')
                            console.log(error);
                    }
                });
            }else{
                swal.fire("Please select user type first");
            }
	    });

        $('#assign_permission').on('submit', function(e) {
		    e.preventDefault();
		    $.ajax({
		        type: "POST",
		        url:"{{url('admin/permission/update-permission')}}",
		        data: $(this).serialize(),
                dataType: 'JSON',
		        success: function(response) {
                   	swal.fire("Updated!", "Permission has been updated successfully", "success");
                    $('#update_user_data').html(response.allUserAccessHTML);
		        },
		        error: function ( xhr, status, error ){
                    console.log(error);
	                // Swal.fire({
	                //     title: "Error!",
	                //     html: message,
	                //     icon:"error",
	                //     showConfirmButton: true
	                // })
	            }
		    });
        });

    });
    </script>
	@stop

@endsection
