<table class="table table-bordered table-striped align-middle" id="update_user_data">
    <thead>
        <tr>
            <th class="text-center">User Name</th>
            @foreach ($componentList as $key=>$value)
                <th>{{ $value }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @if ($users)
            @foreach ($users as $user)
            <tr>
                <td class="text-center">{{ $user->name??$user->name_en }}</td>
                @php
                    $hasPermissions = $user->components->pluck('id')->toArray();
                @endphp
                @foreach ($componentList as $key=>$value)
                    <td>
                        <input type="checkbox" class="checkbox-item" {{ in_array($key, $hasPermissions)?'checked':'' }} disabled />
                    </td>
                @endforeach
            </tr>
            @endforeach
        @else
            <tr>
                <td colspan="6">{{ __("There is no component available") }}</td>
            </tr>
        @endif
    </tbody>
</table>
