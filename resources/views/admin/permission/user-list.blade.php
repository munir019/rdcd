<div class="col-lg-6 col-md-6 col-sm-12">
    {{-- <label for="user">Select User <span class="text-danger">*</span></label> --}}
    {!! Form::select('user', $userList, null, ['class' => 'form-control', 'id' => 'user_id', 'placeholder' => '---Select User---']) !!}
    @error('user')
        <span class="text-danger">{{$message}}</span>
    @enderror
</div>
