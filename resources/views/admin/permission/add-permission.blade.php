@extends('layouts.backendapp')
@section('page_title', 'Assign User Permission to Component')

@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-md-11">
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Assign User Permission to Component
								</h3>
							</div>
						</div>
					</div>
                    <!--begin::Form-->
					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('permission.store') }}" enctype="multipart/form-data" method="POST">
						@csrf
						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    {{-- <label for="user">Select User <span class="text-danger">*</span></label> --}}
                                    {!! Form::select('user', $userList, null, ['class' => 'form-control', 'id' => 'user', 'placeholder' => '---Select user---']) !!}
                                    @error('user')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">
                                </div>
							</div>

							<div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                     <fieldset class="well component-list">
                                        <legend class="well-legend">Select Component</legend>
                                        <div class="col-lg-12">
                                            {{-- <label for="icon">Select Components</label> --}}
                                            @foreach ($componentList as $key=>$value)
                                                {!! Form::checkbox('permissions[]', $key, null, ['class'=>'checkbox-item']) !!}
                                                <span class="checkbox-value">{{ $value }}</span>
                                            @endforeach
                                      </fieldset>
                                </div>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" class="btn btn-form-submit">
									Give Permission
								</button>
								<a href="{{ route('permission.index') }}" class="btn btn-secondary">Cancel</a>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
            <div class="col-md-11" id="user_components">
                {{-- User Access to Component--}}
            </div>
		</div>
	</div>

    @section('scripts')
    <script>
        $('#user_id').change(function(){
            if($(this).val() != ''){
                $.ajax({
                    url: "{{url('admin/user/getPermissionList')}}/"+$(this).val(),
                    dataType: 'json',
                    success: function(data) {
                        $('#user-components').html(data.userComponentHTML);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
	    });

    </script>
	@stop

@endsection
