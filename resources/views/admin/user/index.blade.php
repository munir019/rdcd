@extends('layouts.backendapp')

@section('page_title', 'User List')

@section('content')
    @section('styles')

    @stop

    <div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							User List
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin: Search Form -->
				<div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                    <div class="col-xl-4 col-lg-8 new-item-btn-container">
                        <a href="{{ route('user.create') }}" class="btn btn-new-item">
                            <span>
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <span>
                                    New User
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
			    <!--begin: Datatable -->
                <div class="table-responsive">
                    <table class="table table-bordered yajra-datatable">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Name(Bengali)</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

	@section('scripts')
    <script>
        $(document).ready(function() {
            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('user.index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'name_bn', name: 'name_bn'},
                    {data: 'email', name: 'email'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'created_at', name: 'create'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });

            $(document.body).on('click', '.btn-delete' ,function(){
                var id = $(this).attr('data-id');

                swal({
                    title:"Are You Sure?",
                    text:"You cannot undo this action!",
                    icon:"error",
                    confirmButtonText:"<span><i class='la la-trash'></i><span>Yes, Delete it!</span></span>",
                    confirmButtonClass:"btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon",
                    showCancelButton:!0,
                    cancelButtonText:"<span></i><span>Cancel</span></span>",
                    cancelButtonClass:"btn btn-secondary m-btn m-btn--pill m-btn--icon"
                }).then((confirm) => {
                    if (confirm.value) {
                        $('#deleteForm-'+id).submit();
                    }
                });
            });
        });
    </script>
	@stop
@endsection
