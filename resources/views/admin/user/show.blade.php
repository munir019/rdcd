{{-- @extends('layouts.backendapp') --}}
@extends('layouts.frontendapp')

{{-- @section('page_title', 'User Profile') --}}

@section('content')
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ __('text.beneficiary_profile') }}
                            @php
                                dd($user);
                            @endphp
                        </h3>
                    </div>
                </div>
            </div>
			<div class="m-portlet__body">
                <div class="m-section__content">
                    <table class="table table-bordered m-table">
                        <tbody>
                            <tr>
                                <td><strong>{{ __('name_bengali') }}: </strong>{{ $user->name }}</td>
                                <td><strong>{{ __('name_english') }}: </strong>{{ $user->name_en }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('mobile') }}: </strong>{{ $user->mobile }}</td>
                                <td><strong>{{ __('email') }}: </strong>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('date_of_birth') }}: </strong>{{ $user->date_of_birth }}</td>
                                <td><strong>{{ __('gender') }}: </strong>{{ $user->gender }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('mother_name') }}: </strong>{{ $user->mother_name }}</td>
                                <td><strong>{{ __('mother_name_en') }}: </strong>{{ $user->mother_name_en }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('father_name') }}: </strong>{{ $user->father_name }}</td>
                                <td><strong>{{ __('father_name_en') }}: </strong>{{ $user->father_name_en }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('spouse_name') }}: </strong>{{ $user->spouse_name }}</td>
                                <td><strong>{{ __('spouse_name_en') }}: </strong>{{ $user->spouse_name_en }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('occupation') }}: </strong>{{ $user->occupation }}</td>
                                <td><strong>{{ __('educational_qualification') }}: </strong>{{  }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('nationality') }}: </strong>{{  }}</td>
                                <td><strong>{{ __('nid') }}: </strong>{{ $user->nid }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('passport_number') }}: </strong>{{ $user->passport }}</td>
                                <td><strong>{{ __('birth_certificate_no') }}: </strong>{{ $user->brn }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('passport_number') }}: </strong>{{ $user->passport }}</td>
                                <td><strong>{{ __('tin_no') }}: </strong>{{ $user->tin }}</td>
                            </tr>
                            <tr>
                                <td><strong>{{ __('pre_address') }}: </strong>{{ $user->pre_address }}</td>
                                <td><strong>{{ __('per_address') }}: </strong>{{ $user->per_address }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
			</div>
		</div>
	</div>

@endsection
