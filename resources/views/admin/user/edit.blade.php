@extends('layouts.backendapp')
@section('page_title', 'Create New User')

@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-lg-8 col-md-10">
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Update User Information
								</h3>
							</div>
						</div>
					</div>
                    <!--begin::Form-->
					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('user.update', $user->id) }}" enctype="multipart/form-data" method="POST">
						@csrf
                        @method('PUT')
						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="name">Name (English) <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control m-input{{ $errors->has('name')? ' border-danger' : '' }}" name="name" id="name" value="{{ old('name')??$user->name }}" placeholder="Enter name in english">
                                    @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">
                                    <label for="name_bn">Name (Bengali) <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control m-input{{ $errors->has('name_bn')? ' border-danger' : '' }}" name="name_bn" id="name_bn" value="{{ old('name_bn')??$user->name_bn }}" placeholder="Enter name in bengali">
                                    @error('name_bn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
							</div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control m-input{{ $errors->has('email')? ' border-danger' : '' }}" name="email" id="email" value="{{ old('email')??$user->email }}" placeholder="Enter email">
                                    @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">
                                    <label for="mobile">Mobile</label>
                                    <input type="text" class="form-control m-input{{ $errors->has('mobile')? ' border-danger' : '' }}" name="mobile" id="mobile" value="{{ old('mobile')??$user->mobile }}" placeholder="Enter mobile number">
                                    @error('mobile')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="photo">Profile Photo</label>
                                    <input type="file" class="form-control-file m-input{{ $errors->has('photo')? ' border-danger' : '' }}" id="photo" name="photo" value="">
                                    @error('photo')
                                        <span class="file-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">

                                </div>
                            </div>

							<div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="status">Status <span class="text-danger">*</span></label>
                                    {!! Form::select('status', ['active'=>'Active', 'inactive'=>'Inactive'], $user->status, ['class' => 'form-control', 'id' => 'status', 'placeholder' => 'Select status']) !!}
                                    @error('status')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">
                                    <label for="password">Password <span class="text-danger">*</span></label>
                                    <input type="password" name="password" class="form-control m-input{{ $errors->has('password')? ' border-danger' : '' }}">
                                    @error('password')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
								<a href="{{ route('user.index') }}" class="btn btn-secondary">Cancel</a>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>

    @section('scripts')
    <script>

    </script>
	@stop

@endsection
