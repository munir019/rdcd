<div class="header-container">
    <div class="header-left">
        <a href="/admin/dashboard" class="logo-wrapper" title="Home">
            <span class="icon logo" aria-hidden="true"></span>
        </a>
    </div>
    <div class="header-right top-nav">
        <div class="lang-switcher-wrapper">
            <ul class="lang-menu">
                <li><a class="{{ (session()->get('locale') =='en')?'active':'' }}" href="{{URL::to('changeLanguage/en')}}">English</a></li>
                <li><a class="{{ (session()->get('locale') =='bn')?'active':'' }}" href="{{URL::to('changeLanguage/bn')}}">বাংলা</a></li>
            </ul>
        </div>
        <div class="notification-wrapper">
            <button class="gray-circle-btn dropdown-btn" title="{{ __('text.notification') }}" type="button">
                <span class="sr-only">{{ __('text.user_notification') }}</span>
                <span class="icon notification active" aria-hidden="true"></span>
            </button>
            <ul class="users-item-dropdown notification-dropdown dropdown">
                <li>
                    <a href="##">
                        <div class="notification-dropdown-icon info">
                            <i data-feather="check" aria-hidden="true"></i>
                        </div>
                        <div class="notification-dropdown-text">
                            <span class="notification-dropdown__title">{{ __('text.user_notification') }}</span>
                            <span class="notification-dropdown__subtitle">{{ __('text.no_notification') }}</span>
                        </div>
                    </a>
                </li>
                <li>
                    <a class="link-to-page" href="##">Go to Notifications page</a>
                </li>
            </ul>
        </div>
        <div class="nav-user-wrapper">
            <button href="#" class="nav-user-btn dropdown-btn" title="{{ __('text.myprofile') }}" type="button">
                <span class="sr-only">{{ __('text.myprofile') }}</span>
                <span class="nav-user-img">
                    {{-- <img class="user-profile-image" src="{{ $user->photo??asset('assets/img/user-profile-img.png') }}" /> --}}
                    <img src="{{ $user->photo??asset('assets/img/user-profile-img.png') }}" alt="{{ $user->name??'' }}">
                </span>
            </button>
            <ul class="users-item-dropdown nav-user-dropdown dropdown">
                <li><a href="##">
                    <i data-feather="settings" aria-hidden="true"></i>
                    <span>{{ __('text.account_settings') }}</span>
                </a></li>
                <li>
                    @if(auth('admin')->check())
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a class="danger" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                            <i data-feather="log-out" aria-hidden="true"></i>
                            <span>{{ __('text.logout') }}</span>
                        </a>
                    </form>
                    @else
                        <a class="danger" href="../doptorlogout">
                            <i data-feather="log-out" aria-hidden="true"></i>
                            <span>{{ __('text.logout') }}</span>
                        </a>
                    @endif
                </li>
            </ul>
        </div>
        {{-- <div class="nav-site-switch-wrapper">
            <button class="white-circle-btn dropdown-btn" title="{{ __('text.services_widget') }}" type="button">
                <span class="sr-only">{{ __('text.services_widget') }}</span>
                <span class="icon site-switch-icon active" aria-hidden="true"></span>
            </button>
            <ul class="users-item-dropdown nav-user-dropdown dropdown">
                <li><a href="##">
                    <i data-feather="user" aria-hidden="true"></i>
                    <span>{{ __('text.services_widget') }}</span>
                </a></li>
                <div id="sso-widget"></div>
            </ul>
        </div> --}}
    </div>
</div>
