
<div class="sidebar">
    <div class="sidebar-menu-container">
        <ul class="main-menu">
            <li>
                <a href="{{ route('user.index') }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    {{ __('text.user') }}
                </a>
            </li>
            <li>
                <a href="{{ route('component.index') }}"><i class="fa fa-list" aria-hidden="true"></i>
                    {{ __('text.component_list') }}
                </a>
            </li>
            <li>
                <a href="{{ route('permission') }}"><i class="fa fa-list" aria-hidden="true"></i>
                    {{ __('text.assign_permission') }}
                </a>
            </li>
            {{-- <li>
                <a href="/"><span class="icon ecommerce" aria-hidden="true"></span>আমার ই-কমার্স</a>
            </li>
            <li>
                <a href="/"><span class="icon society" aria-hidden="true"></span>আমার সমিতি</a>
            </li>
            <li>
                <a href="/user"><span class="icon loan" aria-hidden="true"></span></span>আমার ঋণ</a>
            </li>
            <li>
                <a href="/user"><span class="icon training" aria-hidden="true"></span>আমার প্রশিক্ষণ</a>
            </li> --}}
        </ul>
    </div>
</div>
