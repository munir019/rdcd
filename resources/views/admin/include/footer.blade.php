<!-- ! Footer -->
<footer class="footer">
    <div class="footer-left">
        <span class="implement_by">বাস্তবায়নেঃ পল্লী উন্নয়ন ও সমবায় বিভাগ</span>
    </div>
    <div class="footer-center">
        <span>কপিরাইট &copy; ২০২১ - <a href="https://orangebd.com" target="_blank" rel="noopener noreferrer">সর্বস্বত্ত সংরক্ষিত গণপ্রজাতন্ত্রী বাংলাদেশ সরকার</a></span>
    </div>
    <div class="footer-right">
        <span>কারিগরি সহযোগিতায়ঃ</span> <a href="https://orangebd.com" target="_blank"><span class="developed_by"></span></a>
    </div>
</footer>

{{-- <div class="container footer--flex">
    <div class="footer-start">
        <p>2021 © orangebd - <a href="https://orangebd.com" target="_blank"
            rel="noopener noreferrer">orangebd.com</a></p>
    </div>
    <ul class="footer-end">
        <li><a href="##">About</a></li>
        <li><a href="##">Support</a></li>
    </ul>
</div> --}}
