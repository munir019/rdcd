@extends('layouts.backendapp')

@section('page_title', 'Component List')

@section('content')
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Component List
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<!--begin: Search Form -->
				<div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                    <div class="col-xl-4 col-lg-8 new-item-btn-container">
                        <a href="{{ route('component.create') }}" class="btn btn-new-item">
                            <span>
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <span>
                                    New Component
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
				<!--end: Search Form -->
				<!--begin: Datatable -->
                 <div class="table-responsive">
                    <table class="table table-bordered table-striped align-middle" id="ajax_data">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th>Name</th>
                                <th>Title (English)</th>
                                <th>Title (Bengali)</th>
                                <th class="text-center">Component Icon</th>
                                <th class="text-center">URL</th>
                                <th class="text-center">Create</th>
                                <th width="120" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($components)
                                @foreach ($components as $component)
                                <tr>
                                    <td class="text-center">{{ $component->id }}</td>
                                    <td>{{ $component->name }}</td>
                                    <td>{{ $component->title_en }}</td>
                                    <td>{{ $component->title_bn }}</td>
                                    <td class="text-center">
                                        @if ($component->icon)
                                            <img style="max-width: 25px; max-height:25px" src="/images/components/{{$component->icon}}" alt="{{ $component->title_en }}">
                                        @else
                                            <img src="/images/components/defalt.png" alt="{{ $component->title_en }}">
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $component->url }}</td>
                                    <td class="text-center">{{ $component->created_at }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('component.edit', $component->id) }}"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i>&nbsp;&nbsp;</a>
                                        <form method="POST" style="display: inline" action="{{ route('component.destroy', $component->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('component.destroy', $component->id) }}" onclick="event.preventDefault(); this.closest('form').submit();"><i class="fa fa-times fa-1x" aria-hidden="true"></i></a>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">{{ __("There is no component available") }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
				<!--end: Datatable -->
			</div>
		</div>
	</div>

   	@section('scripts')

    <script>
		// $(document.body).on('click', '.btn-delete' ,function(){
		// 	var id = $(this).attr('data-id');

		// 	swal({
		// 		title:"Are You Sure?",
		// 		text:"You cannot undo this action!",
		// 		icon:"error",
		// 		confirmButtonText:"<span><i class='la la-trash'></i><span>Yes, Delete it!</span></span>",
		// 		confirmButtonClass:"btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon",
		// 		showCancelButton:!0,
		// 		cancelButtonText:"<span></i><span>Cancel</span></span>",
		// 		cancelButtonClass:"btn btn-secondary m-btn m-btn--pill m-btn--icon"
		// 	}).then((confirm) => {
		// 		if (confirm.value) {
		// 			$('#deleteForm-'+id).submit();
		// 		}
		// 	});
		// });
		// DatatableJsonRemoteDemo.init()
	//});
    </script>

	@stop
@endsection
