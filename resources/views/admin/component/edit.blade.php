@extends('layouts.backendapp')
@section('page_title', 'Update Component')

@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-md-11">
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">
									Update Component
								</h3>
							</div>
						</div>
					</div>
                    <!--begin::Form-->
					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ route('component.update', $component->id) }}" enctype="multipart/form-data" method="POST">
						@csrf
                        @method('PUT')
						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="title_bn">Title (Bengali) <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control m-input{{ $errors->has('title_bn')? ' border-danger' : '' }}" name="title_bn" id="title_bn" aria-describedby="title_bn" value="{{ old('title_bn')?? $component->title_bn }}" placeholder="Enter title in bengali">
                                    @error('title_bn')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">
                                    <label for="title_en">Title (English) <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control m-input{{ $errors->has('title_en')? ' border-danger' : '' }}" name="title_en" id="title_en" aria-describedby="title_en" value="{{ old('title_en')?? $component->title_en }}" placeholder="Enter title in english">
                                    @error('title_en')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
							</div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="name">Name (English) <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control m-input{{ $errors->has('name')? ' border-danger' : '' }}" name="name" id="name" aria-describedby="name" value="{{ old('name')?? $component->name }}" placeholder="Enter name in english">
                                    @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">
                                    <label for="url">Component Path</label>
                                    <input type="text" class="form-control m-input{{ $errors->has('url')? ' border-danger' : '' }}" name="url" id="url" aria-describedby="url" value="{{ old('url')?? $component->url }}" placeholder="Enter path">
                                    @error('url')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>

							<div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="icon">Component Icon</label>
                                    <input type="file" class="form-control-file edit-file-width m-input{{ $errors->has('icon')? ' border-danger' : '' }}" id="icon" name="icon" value="">
                                    @if (file_exists('images/components/'. $component->icon))
                                        <img class="img-align-right" src="/images/components/{{$component->icon}}" alt="{{ $component->title_en }}">
                                    @endif

                                    @error('icon')
                                        <span class="file-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="col-lg-6">
                                    <label for="status">Status <span class="text-danger">*</span></label>
                                    {!! Form::select('status', ['active'=>'Active', 'inactive'=>'Inactive'], $component->status, ['class' => 'form-control', 'id' => 'status', 'placeholder' => 'Select status']) !!}
                                    @error('status')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button type="submit" class="btn btn-form-submit">
									Submit
								</button>
								<a href="{{ route('component.index') }}" class="btn btn-secondary">Cancel</a>
							</div>
						</div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>

    @section('scripts')
    <script>

    </script>
	@stop

@endsection
