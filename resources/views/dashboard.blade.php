@extends('layouts.backendapp')

@section('content')
<div class="container">
    <h2 class="main-title">{{ __('text.dashboard') }}</h2>
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
            <div class="card text-center">
                <div class="card-header">{{ __('text.loan_and_capital_management') }}</div>
                <div class="card-body">
                    <h5 class="card-title">{{ __('text.user_count') }} ৭</h5>
                    <p class="card-text">সর্বমোট সেবা ১৭ টি</p>
                    <a href="#" class="btn btn-component-link">{{ __('text.component_dashboard') }}</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
            <div class="card text-center">
                <div class="card-header">{{ __('text.cooperative_society_registration_and_monitoring_management') }}</div>
                <div class="card-body">
                    <h5 class="card-title">{{ __('text.user_count') }} ৬৯</h5>
                    <p class="card-text">সর্বমোট সেবা ১১ টি</p>
                    <a href="#" class="btn btn-component-link">{{ __('text.component_dashboard') }}</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
            <div class="card text-center">
                <div class="card-header">{{ __('text.online_milk_collection_and_distribution') }}</div>
                <div class="card-body">
                    <h5 class="card-title">{{ __('text.user_count') }} ৩৫</h5>
                    <p class="card-text">সর্বমোট সেবা ৭ টি</p>
                    <a href="#" class="btn btn-component-link">{{ __('text.component_dashboard') }}</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
            <div class="card text-center">
                <div class="card-header">{{ __('text.sales_ecommerce') }}</div>
                <div class="card-body">
                    <h5 class="card-title">{{ __('text.user_count') }} ১০৯</h5>
                    <p class="card-text">সর্বমোট সেবা ১৫ টি</p>
                    <a href="#" class="btn btn-component-link">{{ __('text.component_dashboard') }}</a>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3">
            <div class="card text-center">
                <div class="card-header">{{ __('text.citizen_training_and_skill_development_management') }}</div>
                <div class="card-body">
                    <h5 class="card-title">{{ __('text.user_count') }} ৫১</h5>
                    <p class="card-text">সর্বমোট সেবা ৯ টি</p>
                    <a href="#" class="btn btn-component-link">{{ __('text.component_dashboard') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
