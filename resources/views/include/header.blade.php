<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-5 col-sm-6 text-start">
                        <a href="{{ route('home') }}" class="logo-wrapper" title="Home">
                            <img class="logo" src="{{ asset('assets/img/logo.png') }}">
                        </a>
                    </div>
                    <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-2 col-sm-0 d-none d-md-block"></div>
                    <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-5 col-sm-6 text-end">
                        <!-- ! Main nav -->
                        @include('partials.topmenu')
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

