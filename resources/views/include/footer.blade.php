<!-- ! Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-4 text-start">
                        <span class="implement_by">বাস্তবায়নেঃ পল্লী উন্নয়ন ও সমবায় বিভাগ</span>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-4 text-center">
                        <span>কপিরাইট &copy; ২০২১ - <a href="https://orangebd.com" target="_blank" rel="noopener noreferrer">সর্বস্বত্ত সংরক্ষিত গণপ্রজাতন্ত্রী বাংলাদেশ সরকার</a></span>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-4 text-end">
                        <span class="align-top">কারিগরি সহযোগিতায়ঃ</span> <a href="https://orangebd.com" target="_blank"><span class="developed_by"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
