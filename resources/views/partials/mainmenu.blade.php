
<div class="sidebar">
    <div class="sidebar-profile">
        <div class="profile-img">
            <img class="user-profile-image" src="{{ $user->photo??asset('assets/img/user-profile-img.png') }}" />
        </div>
        <div class="profile-information">
            <span class="user-name">
                @if(session()->has('employee'))
                    {{  $user->name_bn?? $user->username }}
                @else
                    {{ $user->name??'করিম উদ্দিন' }}</span>
                @endif

            @if (session()->has('citizen'))
                <span class="user-society-name">সুবিধাভোগী পি.এস.কে সমবায় সমিতি</span>
            @endif
            <span class="user-nid"><label for="nid">জাতীয় পরিচয়পত্রঃ </label>{{ $user->nid??'২৩৯৩৮০১৪৩২' }}</span>
            <span class="user-mobile"><label for="mobile">মোবাইলঃ </label>
                @if(session()->has('employee'))
                    {{ $user->personal_mobile ?? '01717789654' }}
                @else
                    {{ $user->mobile ?? '01717789654' }}
                @endif

            </span>
            <span class="user-email"><label for="email">ইমেইলঃ </label>{{ $user->email??'sample@gmail.com' }}</span>
        </div>
    </div>
    <div class="sidebar-menu-container">
        <ul class="main-menu">
            @if (session()->has('employee') || session()->has('citizen'))
                <li class="active">
                    @if(session()->has('employee'))
                        <a href="{{ route('employee.edit', $user->id??'') }}"><span class="icon profile" aria-hidden="true"></span>{{ __('text.profile') }}</a>
                    @elseif(session()->has('citizen'))
                        <a href="{{ route('citizen.edit', $user->id??'') }}"><span class="icon profile" aria-hidden="true"></span>{{ __('text.profile') }}</a>
                    @else
                        <a href="#">
                    @endif
                </li>
                @php
                    $menuItems = Custom::getUserComponents();
                @endphp
                @foreach ($menuItems as $item)
                <li>
                    <a href="{{ url(config('app.url').$user->type.'/verify/'.$item->id) }}"><span class="icon society" aria-hidden="true"></span>{{ $item->title_bn }}</a>
                </li>
                @endforeach
            @endif
            {{-- <li>
                <a href="/"><span class="icon society" aria-hidden="true"></span>আমার সমিতি</a>
            </li> --}}
            {{-- <li>
                <a href="/user"><span class="icon loan" aria-hidden="true"></span></span>আমার ঋণ</a>
            </li>
            <li>
                <a href="/user"><span class="icon training" aria-hidden="true"></span>আমার প্রশিক্ষণ</a>
            </li>
            <li>
                <a href="/user"><span class="icon news" aria-hidden="true"></span>বার্তা এবং ঘোষণা</a>
            </li> --}}
        </ul>
    </div>
</div>
