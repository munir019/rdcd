{{-- <div class="header-container"> --}}
    {{-- <div class="header-left">
        <a href="/home" class="logo-wrapper" title="Home">
            <img class="logo" src="{{ asset('assets/img/logo.png') }}">
        </a>
    </div> --}}
    <div class="header-right top-nav">
        <div class="lang-switcher-wrapper">
            <ul class="lang-menu">
                <li><a class="{{ (session()->get('locale') =='en')?'active':'' }}" href="{{URL::to('changeLanguage/en')}}">English</a></li>
                <li><a class="{{ (session()->get('locale') =='bn')?'active':'' }}" href="{{URL::to('changeLanguage/bn')}}">বাংলা</a></li>
            </ul>
        </div>

        <div class="nav-user-wrapper">
            @if (session()->has('citizen') || session()->has('employee'))
                <button href="#" class="nav-user-btn dropdown-btn" title="{{ __('myprofile') }}" type="button">
                    <span class="sr-only">{{ __('myprofile') }}</span>
                    <span class="nav-user-img">
                        <img src="{{ $user->photo??asset('assets/img/user-profile-img.png') }}" alt="{{ $user->name??'' }}">
                    </span>
                </button>
                <ul class="users-item-dropdown nav-user-dropdown dropdown">
                    <li>
                        @if(session()->has('employee'))
                            <a href="../employee/edit">
                        @elseif(session()->has('citizen'))
                            <a href="../citizen/profile">
                        @else
                            <a href="#">
                        @endif
                            <i data-feather="user" aria-hidden="true"></i>
                            <span>{{ __('text.profile') }}</span>
                        </a>
                    </li>
                    <li><a href="##">
                        <i data-feather="settings" aria-hidden="true"></i>
                        <span>{{ __('text.account_settings') }}</span>
                    </a></li>
                    <li>
                        @if(session()->has('employee'))
                            <a class="danger" href="../doptorlogout">
                        @elseif(session()->has('citizen'))
                            <a class="danger" href="../idplogout">
                        @else
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <a class="danger" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                        @endif
                                    <i data-feather="log-out" aria-hidden="true"></i>
                                    <span>{{ __('text.logout') }}</span>
                                </a>
                            </form>
                    </li>
                </ul>
            @else
                {{-- <button type="button" class="btn btn-rdcdlogin">
                    <a href="{{ route('rdcdlogin') }}">{{ __('text.login') }}</a>
                </button> --}}
                <div class="rdcdlogin-dropdown">
                    <button type="button" class="btn btn-rdcdlogin dropdown-toggle" data-bs-toggle="dropdown">
                        {{ __('text.login') }}
                    </button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('citizenlogin') }}">{{ __('text.citizen_login') }}</a></li>
                        <li><a class="dropdown-item" href="{{ route('employeelogin') }}">{{ __('text.employee_login') }}</a></li>
                    </ul>
                </div>
            @endif
        </div>
        <div class="nav-site-switch-wrapper">
            <button class="white-circle-btn dropdown-btn" title="{{ __('text.services_widget') }}" type="button">
                <span class="sr-only">{{ __('text.services_widget') }}</span>
                <span class="icon site-switch-icon active" aria-hidden="true"></span>
            </button>
            <ul class="users-item-dropdown nav-user-dropdown dropdown">
                <li><a href="##">
                    <i data-feather="user" aria-hidden="true"></i>
                    <span>{{ __('text.services_widget') }}</span>
                </a></li>
                <div id="sso-widget"></div>
            </ul>
        </div>
    </div>
{{-- </div> --}}
