@extends('layouts.login')

@section('content')
    <div class="admin-login">
        <div class="admin-login-logo w-20 h-20 fill-current">
            @include('partials.logo')
        </div>

        @if ($errors->has('email'))
            <div class="alert alert-danger alert-block fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">×</button>
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
        <form class="admin-login-form" method="POST" action="{{ route('login') }}">
            @csrf
            <div>
                <x-label for="email" :value="__('Email')" />
                <input id="email" class="login-form-input w-full" type="email" name="email" required autofocus />
            </div>
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <input id="password" class="login-form-input w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>
            <div class="flex items-center justify-end login-button-container">
                <button class="login-button">
                    {{ __('Log in') }}
                </button>
            </div>
        </form>
    </div>
@endsection


{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>
    </x-auth-card>
</x-guest-layout> --}}
