@extends('layouts.frontendapp')

@section('content')
<div class="container form-container">
    @include('include.message')
    <div class="row">
        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-10 col-sm-12 col-xs-12">
            <div class="page-title">
                {{ __('text.EMPLOYEE_LOGIN') }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-10 col-sm-12 col-xs-12">
            {{-- <div class="page-title col-12">{{ __('text.EMPLOYEE_LOGIN') }}</div> --}}

            <form class="admin-login-form" method="POST" action="{{ route('rdcdauth') }}">
                @csrf
                <div class="row">
                    <label for="username" class="col-3 col-form-label">{{ __('text.USERNAME') }}</label>
                    <input id="username" class="col-6 form-control" type="username" name="username" placeholder="{{ __('text.EMAIL_MOBILE_NID') }}" required autofocus />
                    <input type="hidden" name="usertype" value="employee">
                </div>
                <div class="row mt-4">
                    <label for="password" class="col-3 col-form-label">{{ __('text.PASSWORD') }}</label>
                    <input id="password" class="form-control col-6" type="password" name="password" placeholder="{{ __('text.PASSWORD') }}" required autocomplete="current-password" />
                </div>
                <div class="flex items-center justify-end login-button-container">
                    <button class="btn btn-rdcdlogin">
                        {{ __('Log in') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
