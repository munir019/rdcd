@extends('layouts.login')

@section('content')
<div class="login-main-content">
    <div class="item item-login-logo">
        <span>লগইন</span>
    </div>
    <div class="item item-2">
        <button type="button" class="btn btn-login btn-beneficiary">
            <a href="../idplogin">ইউজার লগইন</a>
        </button>
    </div>
    <div class="item item-or">
        <span>অথবা</span>
    </div>
    <div class="item item-4">
        <button type="button" class="btn btn-login btn-govt-employee">
            <a href="../doptorlogin">প্রশাসনিক লগইন</a>
        </button>
    </div>
    <div class="item item-login-end"></div>
</div>
@endsection
