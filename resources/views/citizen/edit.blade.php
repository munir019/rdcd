@extends('layouts.frontendapp')
@section('page_title', 'Update Profile')

@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" @if(isset($user->id)) action="{{ route('citizen.update', $user->id) }}" @else action="{{ route('citizen.store') }}" @endif enctype="multipart/form-data" method="POST">
                        @csrf
                        @if ($user->id)
                            @method('PUT')
                        @endif
                        <div class="m-portlet m-portlet--tab">
                            <ul class="nav nav-tabs" id="citizenTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="personal-info-tab" data-bs-toggle="tab" data-bs-target="#tab_personal_info" type="button" role="tab" aria-controls="home" aria-selected="true">
                                        <i class="fa fa-info-circle"></i>{{ __('text.personal_information') }}
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="educational-info-tab" data-bs-toggle="tab" data-bs-target="#tab_educational_info" type="button" role="tab" aria-controls="education" aria-selected="false">
                                        <i class="fa fa-graduation-cap"></i>{{ __('text.educational_qualification') }}
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="workplace-info-tab" data-bs-toggle="tab" data-bs-target="#tab_workplace_information" type="button" role="tab" aria-controls="workplace" aria-selected="false">
                                        <i class="fa fa-briefcase"></i>{{ __('text.workplace_information') }}
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="communication-info-tab" data-bs-toggle="tab" data-bs-target="#tab_communication_information" type="button" role="tab" aria-controls="workplace" aria-selected="false">
                                        <i class="fa fa-map-marker"></i>{{ __('text.communication_information') }}
                                    </button>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="tab_personal_info" role="tabpanel" aria-labelledby="tab_personal_info-tab">
									<div id="personal-info">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6 col-md-6">
                                                <label for="name">{{ __('text.name_bengali') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('name')? ' border-danger' : '' }}" name="name" id="name" aria-describedby="name" @if($user->nid_verify) readonly @endif value="{{ old('name') ?? $user->name  }}" placeholder="Enter name in bengali">
                                                @error('name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <label for="name_en">{{ __('text.name_english') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('name_en')? ' border-danger' : '' }}" name="name_en" id="name_en" aria-describedby="name_en" @if($user->nid_verify) readonly @endif value="{{ old('name_en') ?? $user->name_en }}" placeholder="Enter name in english">
                                                @error('name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="mobile">{{ __('text.mobile') }}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('mobile')? ' border-danger' : '' }}" name="mobile" id="mobile" aria-describedby="mobile" @if($user->nid_verify) readonly @endif value="{{ old('mobile')?? $user->mobile }}" placeholder="Enter mobile number">
                                                @error('mobile')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="email">{{ __('text.email') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('email')? ' border-danger' : '' }}" name="email" id="email" aria-describedby="email" value="{{ old('email')?? $user->email }}" placeholder="Enter email">
                                                @error('email')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="date_of_birth">{{ __('text.date_of_birth') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('date_of_birth')? ' border-danger' : '' }}" name="date_of_birth" id="date_of_birth" @if($user->nid_verify) readonly @endif value="{{ $user->date_of_birth }}">
                                                @error('date_of_birth')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="gender">{{ __('text.gender') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('gender')? ' border-danger' : '' }}" name="gender" id="gender" aria-describedby="gender" @if($user->nid_verify) readonly @endif value="{{ old('gender')?? $user->gender }}" placeholder="Select Gender">
                                                @error('gender')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="mother_name">{{ __('text.mother_name') }}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('mother_name')? ' border-danger' : '' }}" name="mother_name" id="mother_name" aria-describedby="mother_name" @if($user->nid_verify) readonly @endif value="{{ old('mother_name')?? $user->mother_name }}" placeholder="Enter mother name in bengali">
                                                @error('mother_name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="mother_name_en">{{ __('text.mother_name_en') }}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('mother_name_en')? ' border-danger' : '' }}" name="mother_name_en" id="mother_name_en" aria-describedby="mother_name_en" @if($user->nid_verify) readonly @endif value="{{ old('mother_name_en')?? $user->mother_name_en }}" placeholder="Enter mother name in english">
                                                @error('mother_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="father_name">{{ __('text.father_name') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('father_name')? ' border-danger' : '' }}" name="father_name" id="father_name" aria-describedby="father_name" @if($user->nid_verify) readonly @endif value="{{ old('father_name')?? $user->father_name }}" placeholder="Enter mother name in bengali">
                                                @error('father_name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="father_name_en">{{ __('text.father_name_en') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('father_name_en')? ' border-danger' : '' }}" name="father_name_en" id="father_name_en" aria-describedby="father_name_en" @if($user->nid_verify) readonly @endif value="{{ old('father_name_en')?? $user->father_name_en }}" placeholder="Enter mother name">
                                                @error('father_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="spouse_name">{{ __('text.spouse_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('spouse_name')? ' border-danger' : '' }}" name="spouse_name" id="spouse_name" aria-describedby="spouse_name" value="{{ old('spouse_name')?? $user->spouse_name }}" placeholder="Enter mother name in bengali">
                                                @error('spouse_name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="spouse_name_en">{{ __('text.spouse_name_en') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('spouse_name_en')? ' border-danger' : '' }}" name="spouse_name_en" id="spouse_name_en" aria-describedby="spouse_name_en" value="{{ old('spouse_name_en')?? $user->spouse_name_en }}" placeholder="Enter mother name">
                                                @error('spouse_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        {{-- <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="occupation">{{ __('text.occupation') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('occupation')? ' border-danger' : '' }}" name="occupation" id="occupation" aria-describedby="occupation" value="{{ old('occupation')?? $user->occupation }}" placeholder="Enter occupation">
                                                @error('occupation')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="educational_qualification">{{ __('text.educational_qualification') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('educational_qualification')? ' border-danger' : '' }}" name="educational_qualification" id="educational_qualification" aria-describedby="educational_qualification" value="{{ old('educational_qualification')?? $user->educational_qualification??'' }}" placeholder="Enter educational qualification">
                                                @error('educational_qualification')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div> --}}

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="nationality">{{ __('text.nationality') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('nationality')? ' border-danger' : '' }}" name="nationality" id="nationality" aria-describedby="nationality" value="{{ old('nationality')?? $user->nationality }}" placeholder="Enter nationality">
                                                @error('nationality')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="nid">{{ __('text.nid') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('nid')? ' border-danger' : '' }}" name="nid" id="nid" aria-describedby="nid" @if($user->nid_verify) readonly @endif value="{{ old('nid')?? $user->nid }}" placeholder="Enter educational qualification">
                                                @error('nid')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <input type="hidden" name="user_id" value="{{ $user->user_id }}">
                                            <input type="hidden" name="nid_verify" value="{{ $user->nid_verify }}">

                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="passport">{{ __('text.passport_number') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('passport')? ' border-danger' : '' }}" name="passport" id="passport" aria-describedby="passport" value="{{ old('passport')?? $user->passport }}" placeholder="Enter passport number">
                                                @error('passport')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="birth_certificate_no">{{ __('text.birth_certificate_no') }} </label>
                                                <input type="text" class="form-control m-input{{ $errors->has('birth_certificate_no')? ' border-danger' : '' }}" name="brn" id="birth_certificate_no" aria-describedby="birth_certificate_no" value="{{ old('brn')?? $user->brn }}" placeholder="Birth certificate no">
                                                @error('birth_certificate_no')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_educational_info" role="tabpanel" aria-labelledby="tab_educational_info-tab">
									<div id="educational-info">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="educational_qualification">{{ __('text.educational_qualification') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('educational_qualification')? ' border-danger' : '' }}" name="educational_qualification" id="educational_qualification" aria-describedby="educational_qualification" value="{{ old('educational_qualification')?? $user->educational_qualification??'' }}" placeholder="Enter educational qualification">
                                                @error('educational_qualification')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                {{-- <label for="name">{{ __('text.name_english') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('name')? ' border-danger' : '' }}" name="name" id="name" aria-describedby="name" value="{{ old('name') ?? $user->name_en  }}" placeholder="Enter name in english">
                                                @error('name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_workplace_information" role="tabpanel" aria-labelledby="tab_workplace_information">
									<div id="workplace-info">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="occupation">{{ __('text.occupation') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('occupation')? ' border-danger' : '' }}" name="occupation" id="occupation" aria-describedby="occupation" value="{{ old('occupation')?? $user->occupation }}" placeholder="Enter occupation">
                                                @error('occupation')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                {{-- <label for="name">{{ __('text.name_english') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('name')? ' border-danger' : '' }}" name="name" id="name" aria-describedby="name" value="{{ old('name') ?? $user->name_en  }}" placeholder="Enter name in english">
                                                @error('name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_communication_information" role="tabpanel" aria-labelledby="tab_communication_information">
									<div id="communication-info">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6 col-md-6">
                                                <label for="pre_address">{{ __('text.present_address') }} <span class="text-danger">*</span></label>
                                                <textarea class="form-control m-input summernote" name="pre_address" id="pre_address" rows="3">{{ $user->pre_address }}</textarea>
                                                @error('pre_address')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <label for="per_address">{{ __('text.permanent_address') }} <span class="text-danger">*</span></label>
                                                <textarea class="form-control m-input summernote" name="per_address" id="per_address" rows="3">{{ $user->per_address }}</textarea>
                                                @error('per_address')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                    <a href="{{ route('citizen.index') }}" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>

    @section('scripts')
    <script>

    </script>
	@stop

@endsection
