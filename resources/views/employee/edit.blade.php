@extends('layouts.frontendapp')
@section('page_title', 'Update Profile')

@section('content')
	<div class="m-content">
		<div class="row">
			<div class="col-md-12">
				<!--begin::Portlet-->
				<div class="m-portlet m-portlet--tab">
                    <!--begin::Form-->
					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" @if(isset($user->id)) action="{{ route('employee.update', $user->id) }}" @else action="{{ route('employee.store') }}" @endif enctype="multipart/form-data" method="POST">
                        @csrf
                        @if(isset($user->id))
                            @method('PUT')
                        @endif
                        <div class="m-portlet m-portlet--tab">
                            <ul class="nav nav-tabs" id="beneficiaryTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="personal-info-tab" data-bs-toggle="tab" data-bs-target="#tab_personal_info" type="button" role="tab" aria-controls="home" aria-selected="true">
                                        <i class="fa fa-info-circle"></i>{{ __('text.personal_information') }}
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="educational-info-tab" data-bs-toggle="tab" data-bs-target="#tab_educational_info" type="button" role="tab" aria-controls="education" aria-selected="false">
                                        <i class="fa fa-graduation-cap"></i>{{ __('text.educational_qualification') }}
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="office-info-tab" data-bs-toggle="tab" data-bs-target="#tab_office_information" type="button" role="tab" aria-controls="office" aria-selected="false">
                                        <i class="fa fa-briefcase"></i>{{ __('text.office_information') }}
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="communication-info-tab" data-bs-toggle="tab" data-bs-target="#tab_communication_information" type="button" role="tab" aria-controls="workplace" aria-selected="false">
                                        <i class="fa fa-map-marker"></i>{{ __('text.communication_information') }}
                                    </button>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="tab_personal_info" role="tabpanel" aria-labelledby="tab_personal_info-tab">
									<div id="personal-info">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6 col-md-6">
                                                <label for="name_bn">{{ __('text.name_bengali') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('name_en')? ' border-danger' : '' }}" name="name_bn" id="name_bn" aria-describedby="name_bn" value="{{ old('name_bn') ?? $user->name_bn }}" placeholder="Enter name in bengali">
                                                @error('name_bn')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <label for="name_en">{{ __('text.name_english') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('name_en')? ' border-danger' : '' }}" name="name_en" id="name_en" aria-describedby="name_en" value="{{ old('name_en') ?? $user->name_en }}" placeholder="Enter name in english">
                                                @error('name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="personal_mobile">{{ __('text.mobile') }}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('personal_mobile')? ' border-danger' : '' }}" name="personal_mobile" id="personal_mobile" aria-describedby="personal_mobile" @if($user->nid) readonly @endif value="{{ old('personal_mobile')?? $user->personal_mobile }}" placeholder="Enter mobile number">
                                                @error('personal_mobile')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="personal_email">{{ __('text.email') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('personal_email')? ' border-danger' : '' }}" name="personal_email" id="personal_email" aria-describedby="personal_email" value="{{ old('personal_email')?? $user->personal_email }}" placeholder="Enter email">
                                                @error('personal_email')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="date_of_birth">{{ __('text.date_of_birth') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('date_of_birth')? ' border-danger' : '' }}" name="date_of_birth" id="date_of_birth" placeholder="Y-m-d" value="{{ $user->date_of_birth }}">
                                                @error('date_of_birth')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="gender">{{ __('text.gender') }} <span class="text-danger">*</span></label>
                                                {!! Form::select('gender', ['male'=>'Male', 'female'=>'Female'], $user->gender, ['class' => "form-control m-input{{ $errors->has('gender')? ' border-danger' : '' }}", 'id' => 'gender', 'placeholder' => 'Select gender']) !!}
                                                {{-- <input type="text" class="form-control m-input{{ $errors->has('gender')? ' border-danger' : '' }}" name="gender" id="gender" aria-describedby="gender" value="{{ old('gender')?? $user->gender }}" placeholder="Select Gender"> --}}
                                                @error('gender')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="mother_name_bn">{{ __('text.mother_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('mother_name_bn')? ' border-danger' : '' }}" name="mother_name_bn" id="mother_name_bn" aria-describedby="mother_name_bn" value="{{ old('mother_name_bn')?? $user->mother_name_bn }}" placeholder="Enter mother name in bengali">
                                                @error('mother_name_bn')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="mother_name_en">{{ __('text.mother_name_en') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('mother_name_en')? ' border-danger' : '' }}" name="mother_name_en" id="mother_name_en" aria-describedby="mother_name_en" value="{{ old('mother_name_en')?? $user->mother_name_en }}" placeholder="Enter mother name in english">
                                                @error('mother_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="father_name_bn">{{ __('text.father_name') }} </label>
                                                <input type="text" class="form-control m-input{{ $errors->has('father_name_bn')? ' border-danger' : '' }}" name="father_name_bn" id="father_name_bn" aria-describedby="father_name_bn" value="{{ old('father_name_bn')?? $user->father_name_bn }}" placeholder="Enter mother name in bengali">
                                                @error('father_name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="father_name_en">{{ __('text.father_name_en') }} </label>
                                                <input type="text" class="form-control m-input{{ $errors->has('father_name_en')? ' border-danger' : '' }}" name="father_name_en" id="father_name_en" aria-describedby="father_name_en" value="{{ old('father_name_en')?? $user->father_name_en }}" placeholder="Enter mother name in english">
                                                @error('father_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="spouse_name_bn">{{ __('text.spouse_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('spouse_name_bn')? ' border-danger' : '' }}" name="spouse_name_bn" id="spouse_name_bn" aria-describedby="spouse_name_bn" value="{{ old('spouse_name_bn')?? ' ' }}" placeholder="Enter spouse name in bengali">
                                                @error('spouse_name_bn')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="spouse_name_en">{{ __('text.spouse_name_en') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('spouse_name_en')? ' border-danger' : '' }}" name="spouse_name_en" id="spouse_name_en" aria-describedby="spouse_name_en" value="{{ old('spouse_name_en')?? ' ' }}" placeholder="Enter spouse name in english">
                                                @error('spouse_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="nationality">{{ __('text.nationality') }} </label>
                                                <input type="text" class="form-control m-input{{ $errors->has('nationality')? ' border-danger' : '' }}" name="nationality" id="nationality" aria-describedby="nationality" value="{{ old('nationality')?? $user->nationality }}" placeholder="Enter nationality">
                                                @error('nationality')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="nid">{{ __('text.nid') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('nid')? ' border-danger' : '' }}" name="nid" id="nid" aria-describedby="nid" @if($user->nid) readonly @endif value="{{ old('nid')?? $user->nid }}" placeholder="Enter nid">
                                                @error('nid')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="passport">{{ __('text.passport_number') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('passport')? ' border-danger' : '' }}" name="passport" id="passport" aria-describedby="passport" value="{{ old('passport')?? $user->passport }}" placeholder="Enter passport number">
                                                @error('passport')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="birth_certificate_no">{{ __('text.birth_certificate_no') }} </label>
                                                <input type="text" class="form-control m-input{{ $errors->has('birth_certificate_no')? ' border-danger' : '' }}" name="brn" id="birth_certificate_no" aria-describedby="birth_certificate_no" value="{{ old('brn')?? $user->brn }}" placeholder="Birth registration no">
                                                @error('birth_certificate_no')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <input type="hidden" name="user_id" value="{{ $user->user_id }}">
                                        <input type="hidden" name="username" value="{{ $user->username }}">
                                        <input type="hidden" name="user_alias" value="{{ $user->user_alias }}">
                                        <input type="hidden" name="user_role_id" value="{{ $user->user_role_id }}">
                                        <input type="hidden" name="is_admin" value="{{ $user->is_admin }}">
                                        <input type="hidden" name="is_email_verified" value="{{ $user->is_email_verified }}">
                                        <input type="hidden" name="status" value="{{ $user->status }}">
                                        <input type="hidden" name="employee_id" value="{{ $user->employee_id }}">
                                        <input type="hidden" name="is_cadre" value="{{ $user->is_cadre }}">
                                        <input type="hidden" name="office_info_id" value="{{ $user->office_info_id }}">
                                        <input type="hidden" name="office_id" value="{{ $user->office_id }}">
                                        <input type="hidden" name="office_unit_id" value="{{ $user->office_unit_id }}">
                                        <input type="hidden" name="office_unit_organogram_id" value="{{ $user->office_unit_organogram_id }}">
                                        <input type="hidden" name="office_head" value="{{ $user->office_head }}">
                                        <input type="hidden" name="organogram_info_id" value="{{ $user->organogram_info_id }}">
                                        <input type="hidden" name="superior_unit_id" value="{{ $user->superior_unit_id }}">
                                        <input type="hidden" name="superior_designation_id" value="{{ $user->superior_designation_id }}">
                                        <input type="hidden" name="ref_origin_unit_org_id" value="{{ $user->ref_origin_unit_org_id }}">
                                        <input type="hidden" name="ref_sup_origin_unit_desig_id" value="{{ $user->ref_sup_origin_unit_desig_id }}">
                                        <input type="hidden" name="ref_sup_origin_unit_id" value="{{ $user->ref_sup_origin_unit_id }}">
                                        <input type="hidden" name="is_unit_admin" value="{{ $user->is_unit_admin }}">
                                        <input type="hidden" name="is_unit_head" value="{{ $user->is_unit_head }}">
                                        <input type="hidden" name="is_office_head" value="{{ $user->is_office_head }}">
                                        <input type="hidden" name="protikolpo_status" value="{{ $user->protikolpo_status }}">

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_educational_info" role="tabpanel" aria-labelledby="tab_educational_info-tab">
									<div id="educational-info">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="educational_qualification">{{ __('text.educational_qualification') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('educational_qualification')? ' border-danger' : '' }}" name="educational_qualification" id="educational_qualification" aria-describedby="educational_qualification" value="{{ old('educational_qualification')?? $user->educational_qualification??'' }}" placeholder="Enter educational qualification">
                                                @error('educational_qualification')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                {{-- <label for="name">{{ __('text.name_english') }} <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control m-input{{ $errors->has('name')? ' border-danger' : '' }}" name="name" id="name" aria-describedby="name" value="{{ old('name') ?? $user->name_en  }}" placeholder="Enter name in english">
                                                @error('name')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_office_information" role="tabpanel" aria-labelledby="tab_office_information">
									<div id="workplace-info">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="designation_bn">{{ __('text.designation') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('designation_bn')? ' border-danger' : '' }}" name="designation_bn" id="designation_bn" aria-describedby="designation_bn" value="{{ old('designation_bn')?? $user->designation_bn }}" placeholder="Enter designation in bengali">
                                                @error('designation_bn')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="designation_en">{{ __('text.designation') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('designation_en')? ' border-danger' : '' }}" name="designation_en" id="designation_en" aria-describedby="designation_en" value="{{ old('designation_en')?? $user->designation_en }}" placeholder="Enter designation in english">
                                                @error('designation_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="short_name_bn">{{ __('text.short_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('short_name_bn')? ' border-danger' : '' }}" name="short_name_bn" id="short_name_bn" aria-describedby="short_name_bn" value="{{ old('short_name_bn')?? $user->short_name_bn }}" placeholder="Enter short name in bengali">
                                                @error('short_name_bn')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="short_name_en">{{ __('text.short_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('short_name_en')? ' border-danger' : '' }}" name="short_name_en" id="short_name_en" aria-describedby="short_name_en" value="{{ old('short_name_en')?? $user->short_name_en }}" placeholder="Enter designation in english">
                                                @error('short_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="incharge_label">{{ __('text.incharge_label') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('incharge_label')? ' border-danger' : '' }}" name="incharge_label" id="incharge_label" aria-describedby="incharge_label" value="{{ old('incharge_label')?? $user->incharge_label }}" placeholder="Enter incharge in bengali">
                                                @error('incharge_label')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-3">
                                                <label for="designation_level">{{ __('text.designation_level') }}</label>
                                                <input type="number" class="form-control m-input{{ $errors->has('designation_level')? ' border-danger' : '' }}" name="designation_level" id="designation_level" aria-describedby="designation_level" value="{{ old('designation_level')?? $user->designation_level }}" placeholder="Enter designation level in english">
                                                @error('designation_level')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-3">
                                                <label for="designation_sequence">{{ __('text.designation_sequence') }}</label>
                                                <input type="number" class="form-control m-input{{ $errors->has('designation_sequence')? ' border-danger' : '' }}" name="designation_sequence" id="designation_sequence" aria-describedby="designation_sequence" value="{{ old('designation_sequence')?? $user->designation_sequence }}" placeholder="Enter designation level in english">
                                                @error('designation_sequence')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="office_name_bn">{{ __('text.office_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('office_name_bn')? ' border-danger' : '' }}" name="office_name_bn" id="office_name_bn" aria-describedby="office_name_bn" value="{{ old('office_name_bn')?? $user->office_name_bn }}" placeholder="Enter office name in bengali">
                                                @error('office_name_bn')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="office_name_en">{{ __('text.office_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('office_name_en')? ' border-danger' : '' }}" name="office_name_en" id="office_name_en" aria-describedby="office_name_en" value="{{ old('office_name_en')?? $user->office_name_en }}" placeholder="Enter office name in english">
                                                @error('office_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="unit_name_bn">{{ __('text.unit_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('unit_name_bn')? ' border-danger' : '' }}" name="unit_name_bn" id="unit_name_bn" aria-describedby="unit_name_bn" value="{{ old('unit_name_bn')?? $user->unit_name_bn }}" placeholder="Enter unit name in bengali">
                                                @error('unit_name_bn')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="unit_name_en">{{ __('text.unit_name') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('unit_name_en')? ' border-danger' : '' }}" name="unit_name_en" id="unit_name_en" aria-describedby="unit_name_en" value="{{ old('unit_name_en')?? $user->unit_name_en }}" placeholder="Enter unit name in english">
                                                @error('unit_name_en')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="joining_date">{{ __('text.joining_date') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('joining_date')? ' border-danger' : '' }}" name="joining_date" id="joining_date" aria-describedby="joining_date" value="{{ old('joining_date')?? $user->joining_date }}" placeholder="Y-m-d">
                                                @error('joining_date')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="joining_date">{{ __('text.joining_date') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('joining_date')? ' border-danger' : '' }}" name="joining_date" id="joining_date" aria-describedby="joining_date" value="{{ old('joining_date')?? $user->joining_date }}" placeholder="Y-m-d">
                                                @error('joining_date')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6">
                                                <label for="last_office_date">{{ __('text.last_office_date') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('last_office_date')? ' border-danger' : '' }}" name="last_office_date" id="last_office_date" aria-describedby="last_office_date" value="{{ old('last_office_date')?? $user->last_office_date }}" placeholder="Y-m-d">
                                                @error('last_office_date')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="last_office_date">{{ __('text.last_office_date') }}</label>
                                                <input type="text" class="form-control m-input{{ $errors->has('last_office_date')? ' border-danger' : '' }}" name="last_office_date" id="last_office_date" aria-describedby="last_office_date" value="{{ old('last_office_date')?? $user->last_office_date }}" placeholder="Y-m-d">
                                                @error('last_office_date')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tab_communication_information" role="tabpanel" aria-labelledby="tab_communication_information">
									<div id="communication-info">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-6 col-md-6">
                                                <label for="pre_address">{{ __('text.present_address') }} </label>
                                                <textarea class="form-control m-input summernote" name="pre_address" id="pre_address" rows="3">{{ old('pre_address')?? $user->pre_address }}</textarea>
                                                @error('pre_address')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <label for="per_address">{{ __('text.permanent_address') }} </label>
                                                <textarea class="form-control m-input summernote" name="per_address" id="per_address" rows="3">{{ old('per_address')?? $user->per_address }}</textarea>
                                                @error('per_address')
                                                    <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                    <a href="{{ route('employee.index') }}" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
					</form>
					<!--end::Form-->
				</div>
				<!--end::Portlet-->
			</div>
		</div>
	</div>

    @section('scripts')
    <script>

    </script>
	@stop

@endsection
