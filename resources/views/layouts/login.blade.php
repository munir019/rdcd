<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Rural Development and Co-operative Division (RDCD)') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Favicon -->
        <link rel="icon" href="{{ asset('assets/img/favicon-32x32.png') }}" type="image/png" sizes="32x32">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/custom_style.css') }}">
    </head>
    <body>
        <div class="main-container">
            {{-- <div class="login-page-header"></div> --}}

            <div class="login-main-wrapper">
                <!-- ! Main -->
                <main class="login-main">
                    {{-- @include('include.message') --}}
                    @yield('content')
                </main>
            </div>

            @include('include.footer')
        </div>

        <!-- Chart library -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-3.5.1.min.js')}}"></script>
        <!-- Custom scripts -->
        {{-- <script src="{{ asset('assets/js/script.js') }}"></script> --}}

        <script>
        $(document).ready(function(){
            $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert-dismissible").alert('close');
            });

            $(".close").click(function(){
                $(this).parent().hide();
            });
        });
        </script>
    </body>
</html>
