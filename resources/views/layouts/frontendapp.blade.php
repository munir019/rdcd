<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Rural Development and Co-operative Division (RDCD)') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Favicon -->
        <link rel="icon" href="<?php echo $baseUrl ?>assets/img/favicon-32x32.png" type="image/png" sizes="32x32">

        <!-- Custom styles -->
        {{-- <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}"> --}}
        <!-- Styles -->
        {{-- <link rel="stylesheet" href="<?php echo $baseUrl ?>css/app.css"> --}}
        <link rel="stylesheet" href="<?php echo $baseUrl ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $baseUrl ?>assets/css/custom_style.css">

        <!-- Scripts -->
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    </head>
    <body>
        <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
        <div class="layer"></div>

        <div class="main-container">
            <!-- ! Main nav -->
            @include('include.header')

            <div class="main-wrapper">

                <!-- ! Sidebar -->
                @include('partials.mainmenu')

                <!-- ! Main -->
                <main class="main">
                    {{-- <div class="main-content"> --}}
                        {{-- @include('include.message') --}}

                        @yield('content')
                    {{-- </div> --}}
                </main>
            </div>

            @include('include.footer')
        </div>

        {{-- <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div> --}}

        <!-- Chart library -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-3.5.1.min.js')}}"></script>
        <script src="{{ asset('assets/plugins/chart.min.js') }}"></script>
        <!-- Icons library -->
        <script src="{{ asset('assets/plugins/feather.min.js') }}"></script>
        <!-- Custom scripts -->
        <script src="{{ asset('assets/js/script.js') }}"></script>
        <!-- Widget scripts -->
        <script src="https://idp.eksheba.gov.bd/js/widget.js?v=1.0.2"></script>

        <script>
            // $('#sso-widget').ssoWidget({'token':'3Lj1bo7vxVtfGhhxtZcO.MnhFeLPNqV'});

            $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert-dismissible").alert('close');
            });
        </script>
    </body>
</html>
