<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Rural Development and Co-operative Division (RDCD)') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Favicon -->
        <link rel="icon" href="<?php echo $baseUrl ?>assets/img/favicon-32x32.png" type="image/png" sizes="32x32">

        <!-- Custom styles -->
        <link rel="stylesheet" href="<?php echo $baseUrl ?>css/app.css">
        {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" rel="stylesheet"> --}}
        <link rel="stylesheet" href="<?php echo $baseUrl ?>assets/plugins/datatable/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $baseUrl ?>assets/plugins/datatable/dataTables.bootstrap5.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $baseUrl ?>assets/plugins/sweetalert2/sweetalert2.css">
        <link rel="stylesheet" href="<?php echo $baseUrl ?>assets/admin/css/custom_style.css">
        <!-- Scripts -->
        {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    </head>
    <body>
        <a class="skip-link sr-only" href="#skip-target">Skip to content</a>
        <div class="layer"></div>

        <div class="main-container">
            <header class="header">
                <!-- ! Main nav -->
                @include('admin.partials.topmenu')
            </header>

            <div class="main-wrapper">

                <!-- ! Sidebar -->
                @include('admin.partials.mainmenu')

                <!-- ! Main -->
                <main class="main">
                    <div class="main-content">
                        @include('admin.include.message')

                        @yield('content')
                    </div>
                </main>
            </div>

            @include('include.footer')
        </div>

        {{-- <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div> --}}

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('assets/js/jquery-3.5.1.min.js')}}"></script>
        {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> --}}
        {{-- <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script> --}}
        <script src="{{ asset('assets/plugins/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/datatable/dataTables.bootstrap5.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/chart.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

        <!-- Icons library -->
        <script src="{{ asset('assets/plugins/feather.min.js') }}"></script>
        <!-- Custom scripts -->
        <script src="{{ asset('assets/js/script.js') }}"></script>
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
		@yield('scripts')
		<!-- END PAGE LEVEL SCRIPTS -->
        <script>
            $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert-dismissible").alert('close');
            });

            $(".close").click(function(){
                $(this).parent().hide();
            });
        </script>

    </body>
</html>
